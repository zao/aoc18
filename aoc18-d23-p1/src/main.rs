#[macro_use]
extern crate lazy_static;

use std::io::{BufRead, Read};
use std::str::FromStr;

use regex::Regex;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Coord {
    x: isize,
    y: isize,
    z: isize,
}

fn manhattan_distance(a: Coord, b: Coord) -> isize {
    (a.x - b.x).abs() + (a.y - b.y).abs() + (a.z - b.z).abs()
}

#[derive(Debug)]
struct Robot {
    pos: Coord,
    r: isize,
}

impl FromStr for Robot {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, (Self::Err)> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r#"^pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)$"#).unwrap();
        }
        let caps = RE.captures(s).unwrap();
        Ok(Robot {
            pos: Coord {
                x: caps[1].parse().unwrap(),
                y: caps[2].parse().unwrap(),
                z: caps[3].parse().unwrap(),
            },
            r: caps[4].parse().unwrap(),
        })
    }
}

fn run_problem<F: Read>(fh: F) -> usize {
    // pos=<-24066954,46054243,51062296>, r=91910926
    let robots = std::io::BufReader::new(fh)
        .lines()
        .map(|line| line.unwrap().parse().unwrap())
        .collect::<Vec<Robot>>();

    let best = robots.iter().max_by_key(|bot| bot.r).unwrap();
    let close_enough = robots
        .iter()
        .filter(|bot| manhattan_distance(best.pos, bot.pos) <= best.r)
        .count();
    
    println!("Close enough: {}", close_enough);

    9001
}

fn main() {
    run_problem(std::fs::File::open("input.txt").unwrap());
    // run_problem(std::fs::File::open("example.txt").unwrap());
}
