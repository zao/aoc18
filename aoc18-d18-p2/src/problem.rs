use std::collections::{HashMap, HashSet};
use std::io::{BufRead, BufReader, Read};

extern crate regex;
use regex::Regex;

pub type Coord = (isize, isize);

pub fn relative_coord(coord: Coord, base: Coord) -> Coord {
    (coord.0 - base.0, coord.1 - base.1)
}

pub fn offset_coord(coord: Coord, offset: Coord) -> Coord {
    (coord.0 + offset.0, coord.1 + offset.1)
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Rect {
    min_x: isize,
    max_x: isize,
    min_y: isize,
    max_y: isize,
}

pub struct RectIterator {
    r: Rect,
    x: isize,
    y: isize,
}

impl Iterator for RectIterator {
    type Item = Coord;

    fn next(&mut self) -> Option<Self::Item> {
        if self.y > self.r.max_y {
            None
        } else {
            let ret = Some((self.x, self.y));
            if self.x == self.r.max_x {
                self.x = self.r.min_x;
                self.y += 1;
            } else {
                self.x += 1;
            }
            ret
        }
    }
}

impl Rect {
    pub fn union(self, rhs: Rect) -> Rect {
        Rect {
            min_x: self.min_x.min(rhs.min_x),
            max_x: self.max_x.max(rhs.max_x),
            min_y: self.min_y.min(rhs.min_y),
            max_y: self.max_y.max(rhs.max_y),
        }
    }

    pub fn top_left(&self) -> Coord {
        (self.min_x, self.min_y)
    }

    pub fn bottom_right(&self) -> Coord {
        (self.max_x, self.max_y)
    }

    pub fn width(&self) -> usize {
        (self.max_x - self.min_x + 1) as usize
    }

    pub fn height(&self) -> usize {
        (self.max_y - self.min_y + 1) as usize
    }

    pub fn iter(&self) -> RectIterator {
        RectIterator {
            r: *self,
            x: self.min_x,
            y: self.min_y,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Grid<T> {
    pub cells: Vec<T>,
    pub width: usize,
    pub height: usize,
}

impl<T: Clone + Default> Grid<T> {
    fn new(width: usize, height: usize) -> Self {
        let mut cells = Vec::new();
        cells.resize(width * height, T::default());
        Self {
            cells,
            width,
            height,
        }
    }

    pub fn get(&self, coord: Coord) -> Option<T> {
        if coord.0 >= 0
            && coord.0 < self.width as isize
            && coord.1 >= 0
            && coord.1 < self.height as isize
        {
            Some(self.cells[coord.1 as usize * self.width + coord.0 as usize].clone())
        } else {
            None
        }
    }

    fn get_mut(&mut self, coord: Coord) -> Option<&mut T> {
        if coord.0 >= 0
            && coord.0 < self.width as isize
            && coord.1 >= 0
            && coord.1 < self.height as isize
        {
            Some(&mut self.cells[coord.1 as usize * self.width + coord.0 as usize])
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Acre {
    Open,
    Wooded,
    Lumberyard,
}

impl Default for Acre {
    fn default() -> Acre {
        Acre::Open
    }
}

#[derive(Debug, Clone)]
pub struct Chart {
    pub stage: Grid<Acre>,
    pub bbox: Rect,
}

impl Chart {
    pub fn new<F: BufRead>(fh: F) -> Chart {
        let mut inputs = Vec::new();
        for line in BufReader::new(fh).lines() {
            let mut row = Vec::new();
            let line = line.unwrap();
            for ch in line.chars() {
                row.push(match ch {
                    '.' => Acre::Open,
                    '#' => Acre::Lumberyard,
                    '|' => Acre::Wooded,
                    _ => panic!(),
                });
            }
            inputs.push(row);
        }

        let bbox = Rect {
            min_x: 0,
            max_x: inputs[0].len() as isize - 1,
            min_y: 0,
            max_y: inputs.len() as isize - 1,
        };

        let mut stage = Grid::new(bbox.width(), bbox.height());
        for (y, row) in inputs.into_iter().enumerate() {
            for (x, acre) in row.into_iter().enumerate() {
                stage.get_mut((x as isize, y as isize)).map(|a| *a = acre);
            }
        }

        Chart { stage, bbox }
    }

    pub fn get(&self, coord: Coord) -> Option<Acre> {
        let rel = relative_coord(coord, self.bbox.top_left());
        self.stage.get(rel)
    }

    fn get_mut(&mut self, coord: Coord) -> Option<&mut Acre> {
        let rel = relative_coord(coord, self.bbox.top_left());
        self.stage.get_mut(rel)
    }
}

fn neighbourhood(coord: Coord) -> Vec<Coord> {
    let x0 = coord.0 - 1;
    let x1 = coord.0;
    let x2 = coord.0 + 1;
    let y0 = coord.1 - 1;
    let y1 = coord.1;
    let y2 = coord.1 + 1;
    vec![
        (x0, y0),
        (x1, y0),
        (x2, y0),
        (x0, y1),
        (x2, y1),
        (x0, y2),
        (x1, y2),
        (x2, y2),
    ]
}

struct Inventory {
    open: usize,
    wooded: usize,
    lumberyards: usize,
}

impl Inventory {
    fn new() -> Self {
        Self {
            open: 0,
            wooded: 0,
            lumberyards: 0,
        }
    }

    fn add_open(self) -> Self {
        Self {
            open: self.open + 1,
            wooded: self.wooded,
            lumberyards: self.lumberyards,
        }
    }

    fn add_wooded(self) -> Self {
        Self {
            open: self.open,
            wooded: self.wooded + 1,
            lumberyards: self.lumberyards,
        }
    }

    fn add_lumberyard(self) -> Self {
        Self {
            open: self.open,
            wooded: self.wooded,
            lumberyards: self.lumberyards + 1,
        }
    }
}

pub fn run_problem<F: Read, Trace>(fh: F, desired_minute: usize, mut trace: Trace) -> usize
where
    Trace: FnMut(&Chart),
{
    let mut chart = Chart::new(BufReader::new(fh));

    let mut last_seen = HashMap::new();

    let mut perform_duplicate_checks = true;
    let mut minute = 0;
    while minute < desired_minute {
        trace(&chart);
        if perform_duplicate_checks {
            last_seen.insert(chart.stage.clone(), minute);
        }
        let mut stage: Grid<Acre> = Grid::new(chart.stage.width, chart.stage.height);

        for mid in chart.bbox.iter() {
            let inv =
                neighbourhood(mid)
                    .into_iter()
                    .fold(Inventory::new(), |inv, coord| {
                        match chart.stage.get(coord) {
                            Some(Acre::Open) => inv.add_open(),
                            Some(Acre::Wooded) => inv.add_wooded(),
                            Some(Acre::Lumberyard) => inv.add_lumberyard(),
                            None => inv,
                        }
                    });
            *stage.get_mut(mid).unwrap() = match chart.stage.get(mid).unwrap() {
                Acre::Open => {
                    if inv.wooded >= 3 {
                        Acre::Wooded
                    } else {
                        Acre::Open
                    }
                }
                Acre::Wooded => {
                    if inv.lumberyards >= 3 {
                        Acre::Lumberyard
                    } else {
                        Acre::Wooded
                    }
                }
                Acre::Lumberyard => {
                    if inv.wooded >= 1 && inv.lumberyards >= 1 {
                        Acre::Lumberyard
                    } else {
                        Acre::Open
                    }
                }
            };
        }

        minute += 1;
        if perform_duplicate_checks {
            if let Some(last_idx) = last_seen.get(&stage) {
                let period = minute - last_idx;
                eprintln!("Duplicate from minute {} found on minute {}, cycle length {}!", last_idx, minute, period);
                let remaining_minutes = desired_minute - minute;
                minute += remaining_minutes / period * period;
                perform_duplicate_checks = false;
            }
        }

        chart.stage = stage;
    }

    trace(&chart);
    let mut wooded_count = 0;
    let mut lumberyard_count = 0;
    for coord in chart.bbox.iter() {
        match chart.get(coord).unwrap() {
            Acre::Wooded => wooded_count += 1,
            Acre::Lumberyard => lumberyard_count += 1,
            _ => (),
        }
    }

    let resource_value = wooded_count * lumberyard_count;
    eprintln!(
        "Resource value: {} * {} = {}",
        wooded_count, lumberyard_count, resource_value
    );
    resource_value
}
