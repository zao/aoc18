extern crate aoc18_d15_p1;
use aoc18_d15_p1::run_problem;

fn main() {
    let conclusion = run_problem(std::fs::File::open("input.txt").unwrap());
    println!("{:#?}", conclusion);
}
