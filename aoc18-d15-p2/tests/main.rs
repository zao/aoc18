extern crate aoc18_d15_p2;
// use aoc18_d15_p2::*;

// use std::collections::HashSet;
// use std::io::{BufRead, BufReader};

// fn graph_from_input(input: &str) -> Graph {
//     let mut edges: Vec<(Coord, Coord)> = Vec::new();
//     let rows = BufReader::new(std::io::Cursor::new(input))
//         .lines()
//         .map(|line| line.unwrap())
//         .collect::<Vec<String>>();
//     for (row, line) in rows.iter().enumerate() {
//         for (col, ch) in line.chars().enumerate() {
//             if ch == '.' {
//                 let e0 = (row as isize, col as isize);
//                 for nb in adjacent4(e0) {
//                     if rows[nb.0 as usize].chars().nth(nb.1 as usize).unwrap() == '.' {
//                         edges.push((e0, nb));
//                     }
//                 }
//             }
//         }
//     }
//     Graph {
//         vertices: Default::default(),
//         edges: edges.into_iter().collect(),
//     }
// }

// #[test]
// fn investigate() {
//     let input = r#"#########
// #.GG....#
// #.E.#...#
// #.G##...#
// #...##..#
// #...#...#
// #.....G.#
// #.....G.#
// #########"#;
//     let graph = graph_from_input(input);
//     let starts = vec![(5, 6), (6, 5), (6, 7)];
//     let targets = vec![(2, 1), (2, 3)];
//     let step = next_step_for_shortest_path(&graph, &starts, &targets);
//     eprintln!("step: {:?}", step);
//     assert!(step.is_some());
// }

// #[test]
// fn inspect_path_loop() {
//     let vertices = vec![
//         (1, 1),
//         (1, 3),
//         (1, 4),
//         (1, 5),
//         (2, 1),
//         (2, 2),
//         (2, 3),
//         (2, 4),
//         (2, 5),
//         (3, 1),
//         (3, 3),
//         (3, 5),
//         (4, 1),
//         (4, 2),
//         (4, 3),
//         (5, 1),
//         (5, 2),
//         (5, 3),
//         (5, 4),
//         (5, 5),
//     ]
//     .into_iter()
//     .collect::<HashSet<Coord>>();
//     let edges = vertices
//         .iter()
//         .map(|&v| {
//             adjacent4(v)
//                 .into_iter()
//                 .filter_map(|a| vertices.get(&a).map(|&a| (v.clone(), a).clone()))
//                 .collect::<Vec<_>>()
//         })
//         .flatten()
//         .collect();
//     // #######
//     // #.G...#
//     // #.....#
//     // #.#.#.#
//     // #...#E#
//     // #.....#
//     // #######
//     let graph = Graph { vertices, edges };
//     {
//         let starts = vec![(1, 1), (1, 3), (2, 2)];
//         let targets = vec![(3, 5), (5, 5)];
//         let step = next_step_for_shortest_path(&graph, &starts, &targets);
//         assert_eq!(step, Some((1, 3)));
//     }
//     {
//         let starts = vec![(3, 5), (5, 5)];
//         let targets = vec![(1, 1), (1, 3), (2, 2)];
//         let step = next_step_for_shortest_path(&graph, &starts, &targets);
//         assert!(step.is_some());
//     }
// }

// #[test]
// fn t1() {
//     assert_eq!(
//         run_problem(std::io::Cursor::new(
//             r#"#######
// #.G...#
// #...EG#
// #.#.#G#
// #..G#E#
// #.....#
// #######"#
//         )),
//         Conclusion {
//             score: 27730,
//             last_full_round: 47,
//             team_health: 590,
//             winning_team: Team::Goblin
//         }
//     );
// }

// #[test]
// fn t2() {
//     assert_eq!(
//         run_problem(std::io::Cursor::new(
//             r#"#######
// #G..#E#
// #E#E.E#
// #G.##.#
// #...#E#
// #...E.#
// #######"#
//         )),
//         Conclusion {
//             score: 36334,
//             last_full_round: 37,
//             team_health: 982,
//             winning_team: Team::Elf
//         }
//     );
// }

// #[test]
// fn t3() {
//     assert_eq!(
//         run_problem(std::io::Cursor::new(
//             r#"#######
// #E..EG#
// #.#G.E#
// #E.##E#
// #G..#.#
// #..E#.#
// #######"#
//         )),
//         Conclusion {
//             score: 39514,
//             last_full_round: 46,
//             team_health: 859,
//             winning_team: Team::Elf
//         }
//     );
// }

// #[test]
// fn t4() {
//     assert_eq!(
//         run_problem(std::io::Cursor::new(
//             r#"#######
// #E.G#.#
// #.#G..#
// #G.#.G#
// #G..#.#
// #...E.#
// #######"#
//         )),
//         Conclusion {
//             score: 27755,
//             last_full_round: 35,
//             team_health: 793,
//             winning_team: Team::Goblin
//         }
//     );
// }

// #[test]
// fn t5() {
//     assert_eq!(
//         run_problem(std::io::Cursor::new(
//             r#"#######
// #.E...#
// #.#..G#
// #.###.#
// #E#G#G#
// #...#G#
// #######"#
//         )),
//         Conclusion {
//             score: 28944,
//             last_full_round: 54,
//             team_health: 536,
//             winning_team: Team::Goblin
//         }
//     );
// }

// #[test]
// fn t6() {
//     assert_eq!(
//         run_problem(std::io::Cursor::new(
//             r#"#########
// #G......#
// #.E.#...#
// #..##..G#
// #...##..#
// #...#...#
// #.G...G.#
// #.....G.#
// #########"#
//         )),
//         Conclusion {
//             score: 18740,
//             last_full_round: 20,
//             team_health: 937,
//             winning_team: Team::Goblin
//         }
//     );
// }
