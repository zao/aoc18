use std::collections::{HashMap, HashSet};
use std::io::{BufRead, Read};

extern crate binary_heap_plus;
use binary_heap_plus::*;

#[derive(Debug, PartialEq)]
pub enum Conclusion {
    Insufficient,
    Sufficient {
        last_full_round: usize,
        score: usize,
    },
}

type PawnId = usize;

#[derive(Debug, Clone)]
enum Tile {
    Floor,
    Wall,
    Pawn(PawnId),
}

pub type Coord = (isize, isize);

fn new_coord(row: isize, col: isize) -> Coord {
    (row, col)
}

pub fn adjacent4(mid: Coord) -> Vec<Coord> {
    vec![(-1, 0), (0, -1), (0, 1), (1, 0)]
        .iter()
        .map(|offset| new_coord(mid.0 + offset.0, mid.1 + offset.1))
        .collect()
}

#[derive(Debug)]
pub struct Graph {
    pub edges: HashSet<(Coord, Coord)>,
}

impl Graph {
    fn edges(&self) -> HashSet<(Coord, Coord)> {
        self.edges.clone()
    }
}

pub fn next_step_for_shortest_path(
    graph: &Graph,
    starts: &[Coord],
    targets: &[Coord],
) -> Option<Coord> {
    // TODO:
    // Run single->many for each start, keep track of shortest path thus far
    // and cut off if exceeding.
    // Group by target, find reader-best source via reader-best target.
    let mut shortest_thus_far = None;
    let mut start_distances = BinaryHeap::new_min();

    for start in starts {
        let edges = graph.edges();
        let targets = targets.iter().cloned().collect::<HashSet<Coord>>();
        let mut distances: HashMap<Coord, usize> = HashMap::new();
        let mut wavefront = BinaryHeap::new_min();
        let mut found_targets = HashSet::new();
        wavefront.push((1, start.clone()));
        while let Some((dist, coord)) = wavefront.pop() {
            if let Some(best_dist) = shortest_thus_far {
                if dist > best_dist {
                    // No point searching further, wavefront is unambigiously
                    // worse due to min-heap ordering.
                    break;
                }
            }
            if !distances.contains_key(&coord) {
                distances.insert(coord, dist);
                if targets.contains(&coord) {
                    found_targets.insert(coord);
                    shortest_thus_far = match shortest_thus_far {
                        Some(best) if dist >= best => shortest_thus_far,
                        _ => Some(dist),
                    }
                }
                edges.iter().filter(|e| e.0 == coord).for_each(|e| {
                    wavefront.push((dist + 1, e.1));
                });
            }
        }

        for target in found_targets {
            let dist = distances[&target];
            start_distances.push((dist, target, start));
        }
    }
    let best = start_distances.pop();
    best.map(|sd| sd.2.clone())
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Team {
    Goblin,
    Elf,
}

impl Team {
    fn from_symbol(ch: char) -> Option<Team> {
        match ch {
            'G' => Some(Team::Goblin),
            'E' => Some(Team::Elf),
            _ => None,
        }
    }

    fn to_symbol(&self) -> char {
        match self {
            Team::Goblin => 'G',
            Team::Elf => 'E',
        }
    }
}

#[derive(Debug, Clone)]
struct Pawn {
    coord: Coord,
    health: usize,
    damage: usize,
    team: Team,
}

#[derive(Debug, Clone)]
struct Field {
    tiles: Vec<Vec<Tile>>,
    pawns: Vec<Option<Pawn>>,
    rounds_completed: usize,
}

impl Field {
    // fn width(&self) -> usize {
    //     match self.tiles.iter().nth(0) {
    //         None => 0,
    //         Some(values) => values.len(),
    //     }
    // }

    // fn height(&self) -> usize {
    //     self.tiles.len()
    // }

    fn walkable_graph(&self) -> Graph {
        let vertices: HashSet<Coord> = self
            .tiles
            .iter()
            .enumerate()
            .map(|(row, values)| {
                values
                    .iter()
                    .enumerate()
                    .filter(|(_, tile)| match tile {
                        Tile::Floor => true,
                        _ => false,
                    })
                    .map(move |(col, _)| new_coord(row as isize, col as isize))
            })
            .flatten()
            .collect();

        let edges: HashSet<(Coord, Coord)> = vertices
            .iter()
            .flat_map(|vert| {
                adjacent4(*vert)
                    .iter()
                    .filter(|nabo| vertices.contains(&nabo))
                    .map(|nabo| (vert.clone(), nabo.clone()))
                    .collect::<Vec<_>>()
            })
            .collect();

        Graph { edges }
    }
}

impl Field {
    fn new<F: BufRead>(fh: F) -> Self {
        let mut pawns = Vec::new();
        let tiles = fh
            .lines()
            .enumerate()
            .map(|(row, line)| {
                line.map(|line| {
                    line.chars()
                        .enumerate()
                        .map(|(col, ch)| match ch {
                            '#' => Tile::Wall,
                            '.' => Tile::Floor,
                            ch => {
                                let id = pawns.len();
                                pawns.push(Some(Pawn {
                                    coord: new_coord(row as isize, col as isize),
                                    health: 200,
                                    damage: 3,
                                    team: Team::from_symbol(ch).unwrap(),
                                }));
                                Tile::Pawn(id)
                            }
                        })
                        .collect()
                })
                .unwrap()
            })
            .collect();
        Self {
            tiles,
            pawns,
            rounds_completed: 0,
        }
    }

    fn set_elf_power(&mut self, power: usize) {
        for pawn in &mut self.pawns {
            if let Some(pawn) = pawn {
                if let Team::Elf = pawn.team {
                    pawn.damage = power;
                }
            }
        }
    }

    fn hostile_neighbours_for(&self, pawn_id: PawnId) -> Vec<PawnId> {
        self.pawns[pawn_id]
            .as_ref()
            .map(|pawn| {
                adjacent4(pawn.coord)
                    .iter()
                    .filter_map(
                        |coord| match self.tiles[coord.0 as usize][coord.1 as usize] {
                            Tile::Pawn(id)
                                if pawn.team != self.pawns[id].as_ref().unwrap().team =>
                            {
                                Some(id)
                            }
                            _ => None,
                        },
                    )
                    .collect()
            })
            .unwrap_or_default()
    }

    fn is_tile_vacant(&self, coord: Coord) -> bool {
        match self.tiles[coord.0 as usize][coord.1 as usize] {
            Tile::Floor => true,
            _ => false,
        }
    }

    fn hostiles_for(&self, pawn_id: PawnId) -> Vec<PawnId> {
        let pawn = self.pawns[pawn_id].clone().unwrap();
        let mut ret = Vec::new();
        for (id, p) in self.pawns.iter().enumerate() {
            if let Some(p) = p {
                if p.team != pawn.team {
                    ret.push(id);
                }
            }
        }
        ret
    }

    fn render_string(&self) -> String {
        let mut ret = String::new();
        self.tiles.iter().for_each(|line| {
            line.iter().for_each(|tile| {
                ret.push(match tile {
                    Tile::Floor => '.',
                    Tile::Wall => '#',
                    Tile::Pawn(id) => self.pawns[*id].as_ref().unwrap().team.to_symbol(),
                });
            });
            ret.push('\n');
        });
        ret
    }

    fn sim_round(&mut self) -> Option<Conclusion> {
        eprintln!("Simulating round {}", self.rounds_completed + 1);
        eprintln!("{}", self.render_string());
        // Find all alive pawns
        let pawns_alive_at_round_start: Vec<PawnId> = {
            let mut pawns = self
                .pawns
                .iter()
                .enumerate()
                .filter(|(_, p)| p.is_some())
                .map(|(i, p)| (i, p.as_ref().unwrap().coord.clone()))
                .collect::<Vec<_>>();
            pawns.sort_by_key(|t| t.1);
            pawns.iter().map(|p| p.0).collect()
        };

        for pawn_id in pawns_alive_at_round_start {
            // Skip this pawn if it has died during the round.
            let pawn = self.pawns[pawn_id].clone();
            if pawn.is_none() {
                continue;
            }
            let pawn = pawn.unwrap();

            // See if we've won and if so, return a conclusion.
            {
                let remaining_teams = {
                    self.pawns.iter().fold(HashSet::new(), |mut h, p| {
                        if let Some(p) = p {
                            h.insert(p.team);
                        }
                        h
                    })
                };
                if remaining_teams.len() == 1 {
                    let remaining_health = self
                        .pawns
                        .iter()
                        .fold(0, |acc, p| acc + p.clone().map(|p| p.health).unwrap_or(0));
                    return Some(Conclusion::Sufficient {
                        last_full_round: self.rounds_completed,
                        score: self.rounds_completed * remaining_health,
                    });
                }
            }

            let mut hostile_neighbours = self.hostile_neighbours_for(pawn_id);
            if hostile_neighbours.is_empty() {
                // When no enemy is directly adjacent, determine the best adjacent
                // square to move to in order to get to a square adjacent to an
                // enemy.

                let hostiles = self.hostiles_for(pawn_id);
                let open_enemy_adjacent_tiles = hostiles
                    .iter()
                    .flat_map(|id| {
                        let h = self.pawns[*id].clone().unwrap();
                        let a4 = adjacent4(h.coord);
                        a4.iter()
                            .filter(|&&a| self.is_tile_vacant(a))
                            .cloned()
                            .collect::<Vec<Coord>>()
                    })
                    .collect::<Vec<Coord>>();

                let open_adjacent_tiles = adjacent4(pawn.coord)
                    .iter()
                    .filter(|&&c| self.is_tile_vacant(c))
                    .cloned()
                    .collect::<Vec<Coord>>();

                // Find the distances to all enemy-adjacent squares and determine
                // which such square has the lowest cost, if several favour the
                // one first in reading-order.
                if let Some(step) = next_step_for_shortest_path(
                    &self.walkable_graph(),
                    &open_adjacent_tiles,
                    &open_enemy_adjacent_tiles,
                ) {
                    // Move a step along the path.
                    let new_coord = step;
                    self.tiles[pawn.coord.0 as usize][pawn.coord.1 as usize] = Tile::Floor;
                    self.tiles[new_coord.0 as usize][new_coord.1 as usize] = Tile::Pawn(pawn_id);
                    self.pawns[pawn_id].as_mut().unwrap().coord = new_coord;

                    // As we moved, check again for nearby hostiles.
                    hostile_neighbours = self.hostile_neighbours_for(pawn_id);
                }
            }

            // Hit up to one adjacent enemy with lowest hitpoints, tie-break with
            // reading-order.
            if !hostile_neighbours.is_empty() {
                let target_id = hostile_neighbours
                    .iter()
                    .map(|&nb_id| {
                        let nb = self.pawns[nb_id].as_ref().unwrap();
                        (nb.health, nb.coord, nb_id)
                    })
                    .min()
                    .unwrap()
                    .2;
                let mut target = self.pawns[target_id].clone().unwrap();
                target.health = target.health.saturating_sub(pawn.damage);
                if target.health > 0 {
                    self.pawns[target_id] = Some(target);
                } else {
                    // If target dies, remove target.
                    self.pawns[target_id] = None;
                    self.tiles[target.coord.0 as usize][target.coord.1 as usize] = Tile::Floor;
                    if let Team::Elf = target.team {
                        return Some(Conclusion::Insufficient);
                    }
                }
            }
        }

        // If we reach the end of a round, it's completed and we have yet to end.
        self.rounds_completed += 1;
        None
    }
}

pub fn run_problem<F: Read>(fh: F) -> Conclusion {
    let f = Field::new(std::io::BufReader::new(fh));
    for i in 4.. {
        let mut f = f.clone();
        f.set_elf_power(i);
        loop {
            if let Some(conclusion) = f.sim_round() {
                if let Conclusion::Insufficient = conclusion {
                    break;
                } else {
                    return conclusion;
                }
            }
        }
    }
    unreachable!();
}
