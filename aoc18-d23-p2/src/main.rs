#[macro_use]
extern crate lazy_static;

use std::io::{BufRead, Read};
use std::str::FromStr;

use regex::Regex;

use binary_heap_plus::*;

/// * X+ right
/// * Y+ down
/// * Z+ into
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Coord {
    pub x: isize,
    pub y: isize,
    pub z: isize,
}

fn manhattan_distance(a: Coord, b: Coord) -> isize {
    (a.x - b.x).abs() + (a.y - b.y).abs() + (a.z - b.z).abs()
}

#[derive(Debug)]
struct Bot {
    pos: Coord,
    radius: isize,
}

impl FromStr for Bot {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, (Self::Err)> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r#"^pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)$"#).unwrap();
        }
        let caps = RE.captures(s).unwrap();
        Ok(Bot {
            pos: Coord {
                x: caps[1].parse().unwrap(),
                y: caps[2].parse().unwrap(),
                z: caps[3].parse().unwrap(),
            },
            radius: caps[4].parse().unwrap(),
        })
    }
}

#[derive(Debug, Clone, Copy, Ord, PartialOrd, Eq, PartialEq, Hash)]
struct Region {
    min: Coord,
    max: Coord,
}

impl Region {
    /// Constructs a new Region from any two points, not necessarily the min/max corners.
    fn new(a: Coord, b: Coord) -> Self {
        Region {
            min: Coord {
                x: a.x.min(b.x),
                y: a.y.min(b.y),
                z: a.z.min(b.z),
            },
            max: Coord {
                x: a.x.max(b.x),
                y: a.y.max(b.y),
                z: a.z.max(b.z),
            },
        }
    }

    fn singleton(c: Coord) -> Self {
        Region { min: c, max: c }
    }

    /// Determines if a point is inside the region.
    fn contains(&self, p: Coord) -> bool {
        p.x >= self.min.x
            && p.x <= self.max.x
            && p.y >= self.min.y
            && p.y <= self.max.y
            && p.z >= self.min.z
            && p.z <= self.max.z
    }

    fn width(&self) -> usize {
        (self.max.x - self.min.x + 1) as usize
    }

    fn height(&self) -> usize {
        (self.max.y - self.min.y + 1) as usize
    }

    fn depth(&self) -> usize {
        (self.max.z - self.min.z + 1) as usize
    }

    fn iter(&self) -> RegionAreaIterator {
        RegionAreaIterator { i: 0, r: self }
    }
}

struct RegionAreaIterator<'a> {
    i: usize,
    r: &'a Region,
}

impl<'a> Iterator for RegionAreaIterator<'a> {
    type Item = Coord;

    fn next(&mut self) -> Option<Self::Item> {
        let r = self.r;
        if self.i == r.width() * r.height() * r.depth() {
            None
        } else {
            let rx = self.i % r.width();
            let ry = (self.i / r.width()) % r.height();
            let rz = (self.i / r.height() / r.width()) % r.depth();
            self.i += 1;
            Some(Coord {
                x: r.min.x + rx as isize,
                y: r.min.y + ry as isize,
                z: r.min.z + rz as isize,
            })
        }
    }
}

struct DABB {
    c: Coord,
    r: usize,
}

impl DABB {
    fn new(c: Coord, r: usize) -> Self {
        DABB { c, r }
    }
}

impl Region {
    fn intersects_dabb(&self, b: &DABB) -> bool {
        let s = b.c;
        let min = self.min;
        let max = self.max;
        let mut dist = b.r as isize;
        if s.x < min.x {
            dist -= (s.x - min.x).abs();
        } else if s.x > max.x {
            dist -= (s.x - max.x).abs();
        }
        if s.y < min.y {
            dist -= (s.y - min.y).abs();
        } else if s.y > max.y {
            dist -= (s.y - max.y).abs();
        }
        if s.z < min.z {
            dist -= (s.z - min.z).abs();
        } else if s.z > max.z {
            dist -= (s.z - max.z).abs();
        }

        dist > 0
    }

    fn wide_split(&self) -> (Self, Self) {
        let w = self.width() as isize;
        (
            Region::new(
                self.min,
                Coord {
                    x: self.min.x + w / 2 - 1,
                    y: self.max.y,
                    z: self.max.z,
                },
            ),
            Region::new(
                Coord {
                    x: self.min.x + w / 2,
                    y: self.min.y,
                    z: self.min.z,
                },
                self.max,
            ),
        )
    }

    fn high_split(&self) -> (Self, Self) {
        let h = self.height() as isize;
        (
            Region::new(
                self.min,
                Coord {
                    x: self.max.x,
                    y: self.min.y + h / 2 - 1,
                    z: self.max.z,
                },
            ),
            Region::new(
                Coord {
                    x: self.min.x,
                    y: self.min.y + h / 2,
                    z: self.min.z,
                },
                self.max,
            ),
        )
    }

    fn deep_split(&self) -> (Self, Self) {
        let d = self.depth() as isize;
        (
            Region::new(
                self.min,
                Coord {
                    x: self.max.x,
                    y: self.max.y,
                    z: self.min.z + d / 2 - 1,
                },
            ),
            Region::new(
                Coord {
                    x: self.min.x,
                    y: self.min.y,
                    z: self.min.z + d / 2,
                },
                self.max,
            ),
        )
    }
}

fn score_region_conservative(region: &Region, bots: &[Bot]) -> usize {
    let overlap_count = bots
        .iter()
        .filter(|bot| region.intersects_dabb(&DABB::new(bot.pos, bot.radius as usize)))
        .count();

    overlap_count
}

fn score_region_exact(region: &Region, bots: &[Bot]) -> (usize, Coord) {
    let best_hit = region
        .iter()
        .map(|coord| {
            (
                bots.iter()
                    .filter(|bot| manhattan_distance(coord, bot.pos) <= bot.radius)
                    .count(),
                coord,
            )
        })
        .max()
        .unwrap();
    best_hit
}

fn split_longest(region: &Region) -> (Region, Region) {
    let longest_axis = vec![region.width(), region.height(), region.depth()]
        .iter()
        .enumerate()
        .max_by_key(|(_, &dim)| dim)
        .unwrap()
        .0;
    match longest_axis {
        0 => region.wide_split(),
        1 => region.high_split(),
        2 => region.deep_split(),
        _ => unreachable!(),
    }
}

const ORIGIN: Coord = Coord { x: 0, y: 0, z: 0 };

fn find_best_iterative(
    best_count: &mut usize,
    best_dist: &mut Option<isize>,
    region: &Region,
    bots: &[Bot],
) {
    let mut wavefront: BinaryHeap<(usize, Region), _> = BinaryHeap::new();
    wavefront.push((score_region_conservative(region, bots), *region));
    while let Some((upper_bound, region)) = wavefront.pop() {
        if upper_bound < *best_count {
            eprint!("-");
            return;
        }
        eprint!("+");
        if region.width() < 16 && region.height() < 16 && region.depth() < 16 {
            let (score, pos) = score_region_exact(&region, bots);
            if score > 0 {
                let dist = manhattan_distance(ORIGIN, pos);
                if score > *best_count || score == *best_count && dist < best_dist.unwrap() {
                    eprintln!(
                        "New best score {} at distance {} with position {:?}",
                        score, dist, pos
                    );
                    *best_count = score;
                    *best_dist = Some(dist);
                }
            }
        } else {
            let (h1, h2) = split_longest(&region);
            // eprintln!("{:?}\n{:?}", h1, h2);
            // eprintln!("0x{:X} x 0x{:X} x 0x{:X}, 0x{:X} x 0x{:X} x 0x{:X}", h1.width(), h1.height(), h1.depth(), h2.width(), h2.height(), h2.depth());
            wavefront.push((score_region_conservative(&h1, bots), h1));
            wavefront.push((score_region_conservative(&h2, bots), h2));
        }
    }
}

fn find_best_recursive(
    best_count: &mut usize,
    best_dist: &mut Option<isize>,
    region: &Region,
    bots: &[Bot],
) {
    let upper_bound = bots
        .iter()
        .filter(|bot| region.intersects_dabb(&DABB::new(bot.pos, bot.radius as usize)))
        .count();
    if upper_bound < *best_count {
        eprint!("-");
        return;
    }
    eprint!("+");
    if region.width() < 16 && region.height() < 16 && region.depth() < 16 {
        let (score, pos) = score_region_exact(region, bots);
        if score > 0 {
            let dist = manhattan_distance(ORIGIN, pos);
            if score > *best_count || score == *best_count && dist < best_dist.unwrap() {
                eprintln!(
                    "New best score {} at distance {} with position {:?}",
                    score, dist, pos
                );
                *best_count = score;
                *best_dist = Some(dist);
            }
        }
    } else {
        let (h1, h2) = split_longest(region);
        // eprintln!("{:?}\n{:?}", h1, h2);
        // eprintln!("0x{:X} x 0x{:X} x 0x{:X}, 0x{:X} x 0x{:X} x 0x{:X}", h1.width(), h1.height(), h1.depth(), h2.width(), h2.height(), h2.depth());
        find_best_recursive(best_count, best_dist, &h1, bots);
        find_best_recursive(best_count, best_dist, &h2, bots);
    }
}

fn find_best_distance(bots: &[Bot]) -> (usize, usize) {
    let region = {
        let bounds = bots.iter().fold((bots[0].pos, bots[0].pos), |bounds, bot| {
            (
                Coord {
                    x: bounds.0.x.min(bot.pos.x),
                    y: bounds.0.y.min(bot.pos.y),
                    z: bounds.0.z.min(bot.pos.z),
                },
                Coord {
                    x: bounds.1.x.max(bot.pos.x),
                    y: bounds.1.y.max(bot.pos.y),
                    z: bounds.1.z.max(bot.pos.z),
                },
            )
        });
        let pad_width = ((bounds.1.x - bounds.0.x) as usize).next_power_of_two() as isize;
        let pad_height = ((bounds.1.y - bounds.0.y) as usize).next_power_of_two() as isize;
        let pad_depth = ((bounds.1.z - bounds.0.z) as usize).next_power_of_two() as isize;
        eprintln!("{} {} {}", pad_width, pad_height, pad_depth);
        Region::new(
            bounds.0,
            Coord {
                x: bounds.0.x + pad_width - 1,
                y: bounds.0.y + pad_height - 1,
                z: bounds.0.z + pad_depth - 1,
            },
        )
    };
    eprintln!("{:?}", region);
    eprintln!(
        "0x{:X} x 0x{:X} x 0x{:X}",
        region.width(),
        region.height(),
        region.depth()
    );

    let best = bots.iter().max_by_key(|bot| bot.radius).unwrap();
    let close_enough = bots
        .iter()
        .filter(|bot| manhattan_distance(best.pos, bot.pos) <= best.radius)
        .count();

    println!("Close enough: {}", close_enough);

    let whole_score = score_region_conservative(&region, &bots);
    eprintln!("Whole score: {}", whole_score);

    let mut best_count_thus_far = 0;
    let mut best_dist_thus_far = None;
    for bot in bots {
        let baseline = score_region_exact(&Region::singleton(bot.pos), &bots);
        let dist = manhattan_distance(ORIGIN, bot.pos);
        if baseline.0 > best_count_thus_far
            || baseline.0 == best_count_thus_far && dist < best_dist_thus_far.unwrap()
        {
            best_count_thus_far = baseline.0;
            best_dist_thus_far = Some(dist);
        }
    }
    eprintln!(
        "initial best count/dist: {}/{:?}",
        best_count_thus_far, best_dist_thus_far
    );

    find_best_iterative(
        &mut best_count_thus_far,
        &mut best_dist_thus_far,
        &region,
        &bots,
    );
    eprintln!("best: {} @ {:?}", best_count_thus_far, best_dist_thus_far);

    // let halves = region.wide_split();
    // eprintln!("halves: {:?}", halves);
    // halves.iter().for_each(|region| {
    //     eprintln!("region: {:?}", region);
    //     let inside_count = bots.iter().filter(|bot| region.contains(bot.pos)).count();
    //     let overlaps = bots
    //         .iter()
    //         .filter(|bot| region.intersects_dabb(&DABB::new(bot.pos, bot.radius as usize)))
    //         .count();
    //     eprintln!("inside: {:?}, overlaps: {:?}", inside_count, overlaps);
    // });

    (best_count_thus_far, best_dist_thus_far.unwrap() as usize)
}

fn run_problem<F: Read>(fh: F) -> usize {
    // pos=<-24066954,46054243,51062296>, r=91910926
    let bots = std::io::BufReader::new(fh)
        .lines()
        .map(|line| line.unwrap().parse().unwrap())
        .collect::<Vec<Bot>>();

    let best = find_best_distance(&bots);
    eprintln!("best count/distance: {:?}", best);

    best.1 as usize
}

fn main() {
    run_problem(std::fs::File::open("input.txt").unwrap());
    // run_problem(std::fs::File::open("example.txt").unwrap());
}
