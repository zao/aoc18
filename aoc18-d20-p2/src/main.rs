use std::collections::{HashMap, HashSet};

extern crate binary_heap_plus;
use binary_heap_plus::*;

#[derive(Debug, Clone, Copy)]
enum Sym {
    Start,
    End,
    Open,
    Close,
    Choice,
    N,
    W,
    E,
    S,
}

impl Sym {
    fn from_byte(b: u8) -> Self {
        match b {
            b'^' => Sym::Start,
            b'$' => Sym::End,
            b'N' => Sym::N,
            b'W' => Sym::W,
            b'E' => Sym::E,
            b'S' => Sym::S,
            b'(' => Sym::Open,
            b')' => Sym::Close,
            b'|' => Sym::Choice,
            _ => panic!(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Direction {
    North,
    West,
    East,
    South,
}

type Coord = (isize, isize);

fn coord_neighbour(c: Coord, dir: Direction) -> Coord {
    match dir {
        Direction::North => (c.0, c.1 + 1),
        Direction::West => (c.0 - 1, c.1),
        Direction::East => (c.0 + 1, c.1),
        Direction::South => (c.0, c.1 - 1),
    }
}

enum SymRef {
    Forward(usize),
    Backward(usize),
}

impl SymRef {
    fn gather(syms: &[Sym]) -> Option<Vec<Option<SymRef>>> {
        let mut stack = Vec::new();
        let refs = syms
            .iter()
            .enumerate()
            .map(|(i, sym)| match sym {
                Sym::Start | Sym::Open { .. } => {
                    stack.push(i);
                    Some(SymRef::Forward(i))
                }
                Sym::Choice => Some(SymRef::Backward(*stack.last()?)),
                Sym::Close | Sym::End => Some(SymRef::Backward(stack.pop()?)),
                _ => None,
            })
            .collect();
        Some(refs)
    }
}

fn sym_direction(sym: Sym) -> Option<Direction> {
    match sym {
        Sym::N => Some(Direction::North),
        Sym::W => Some(Direction::West),
        Sym::E => Some(Direction::East),
        Sym::S => Some(Direction::South),
        _ => None,
    }
}

fn sweep(syms: &[Sym], empty_choices: &HashSet<usize>) -> Vec<Edge> {
    let mut edges: HashSet<Edge> = HashSet::new();
    let n = syms.len();
    assert!(n > 0);
    let mut clone_stack = Vec::new();
    let mut retire_stack = Vec::new();
    let mut bundle: HashSet<Coord> = HashSet::new();
    bundle.insert((0, 0));
    for i in 1..n {
        let mut added = vec![];
        let sym = syms[i];
        match sym {
            Sym::Start | Sym::End => (),
            Sym::N | Sym::W | Sym::E | Sym::S => {
                let dir = sym_direction(sym).unwrap();
                let mut next_bundle = HashSet::new();
                for coord in bundle {
                    let adj = coord_neighbour(coord, dir);
                    added.push(adj);
                    edges.insert((coord, adj));
                    edges.insert((adj, coord));
                    next_bundle.insert(adj);
                }
                bundle = next_bundle;
            }
            Sym::Open => {
                clone_stack.push(bundle.clone());
                if empty_choices.contains(&i) {
                    retire_stack.push(bundle.clone());
                } else {
                    retire_stack.push(HashSet::new());
                }
            }
            Sym::Choice => {
                retire_stack.last_mut().map(|v| {
                    for coord in bundle {
                        v.insert(coord);
                    }
                });
                bundle = clone_stack.last().unwrap().clone();
            }
            Sym::Close => {
                clone_stack.pop().unwrap();
                bundle = retire_stack.pop().unwrap();
            }
        }
        // eprintln!("After {} {:?}\n clone: {:?}\n bundle: {:?}\n added: {:?}\n retire: {:?}", i, sym, clone_stack, bundle, added, retire_stack);
    }
    edges.into_iter().collect()
}

fn print_map<'a, RI>(rooms: RI, edges: &[Edge])
where
    RI: Iterator<Item = &'a Coord> + Clone,
{
    // eprintln!("Rooms: {:?}", rooms);
    let min_x = rooms.clone().min_by_key(|c| c.0).unwrap().0;
    let max_x = rooms.clone().max_by_key(|c| c.0).unwrap().0;
    let min_y = rooms.clone().min_by_key(|c| c.1).unwrap().1;
    let max_y = rooms.clone().max_by_key(|c| c.1).unwrap().1;
    let width = 1 + 2 * (max_x - min_x + 1) as usize;
    let height = 1 + 2 * (max_y - min_y + 1) as usize;

    eprintln!(
        "x: ({} to {}), y: ({} to {}), width: {}, height: {}",
        min_x, max_x, min_y, max_y, width, height
    );

    {
        let mut canvas: Vec<Vec<char>> = Vec::with_capacity(height);
        {
            let mut empty_line = Vec::with_capacity(width);
            empty_line.resize(width, '#');
            canvas.resize(height, empty_line);
        }

        for room in rooms {
            let x = (room.0 - min_x) as usize;
            let y = (max_y - room.1) as usize;

            let adjacent = edges
                .iter()
                .filter(|e| e.0 == *room)
                .map(|e| e.1)
                .collect::<Vec<Coord>>();
            let has_neighbour = |dir| adjacent.iter().any(|c| *c == coord_neighbour(*room, dir));
            let door_east = has_neighbour(Direction::East);
            let door_south = has_neighbour(Direction::South);
            canvas[1 + 2 * y][1 + 2 * x] = if room.0 == 0 && room.1 == 0 { 'X' } else { '.' };
            if door_east {
                canvas[1 + 2 * y][2 + 2 * x] = '|';
            }
            if door_south {
                canvas[2 + 2 * y][1 + 2 * x] = '-';
            }
        }
        for line in canvas {
            eprintln!("{}", line.into_iter().collect::<String>());
        }
    }
}

type Edge = (Coord, Coord);

fn find_shortest_paths_at_least(edges: &[Edge], length_cutoff: usize) -> usize {
    let mut visited: HashMap<Coord, usize> = HashMap::new();
    let mut wavefront: BinaryHeap<(usize, Coord), _> = BinaryHeap::new_min();
    wavefront.push((0, (0, 0)));

    while let Some((dist, node)) = wavefront.pop() {
        visited.entry(node).or_insert_with(|| {
            for edge in edges.iter().filter(|edge| edge.0 == node) {
                wavefront.push((dist + 1, edge.1));
            }
            dist
        });
    }

    visited.iter().filter(|(_, v)| **v >= length_cutoff).count()
}

fn run_problem(input: std::io::Result<String>) -> std::io::Result<()> {
    let sym_seq = input?
        .as_bytes()
        .iter()
        .cloned()
        .map(Sym::from_byte)
        .collect::<Vec<Sym>>();

    let sym_refs = SymRef::gather(&sym_seq).unwrap();
    let empty_choices = sym_refs.windows(2).fold(HashSet::new(), |mut h, w| {
        match (&w[0], &w[1]) {
            (Some(SymRef::Forward(o)), Some(SymRef::Backward(_))) => {
                h.insert(*o);
            }
            (Some(SymRef::Backward(o1)), Some(SymRef::Backward(o2))) if o1 == o2 => {
                h.insert(*o1);
            }
            _ => (),
        }
        h
    });

    let edges = sweep(&sym_seq, &empty_choices);

    let rooms = edges.iter().fold(HashSet::new(), |mut h, e| {
        h.insert(e.0);
        h.insert(e.1);
        h
    });

    // print_map(rooms.iter(), &edges);

    let lengths = find_shortest_paths_at_least(&edges, 1000);
    eprintln!("Shortest paths of at least 1000 doors: {}", lengths);

    Ok(())
}

fn main() {
    run_problem(Ok("^WNE$".to_owned())).unwrap();
    run_problem(Ok("^ENWWW(NEEE|SSE(EE|N))$".to_owned())).unwrap();
    run_problem(Ok("^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$".to_owned())).unwrap();
    run_problem(Ok(
        "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$".to_owned()
    ))
    .unwrap();
    run_problem(Ok(
        "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$".to_owned(),
    ))
    .unwrap();
    run_problem(std::fs::read_to_string("input.txt")).unwrap();
}
