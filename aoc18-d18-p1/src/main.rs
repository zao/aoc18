pub mod problem;
use crate::problem::*;

extern crate minifb;
use minifb::{Key, KeyRepeat, Scale, Window, WindowOptions};

fn render_chart(chart: &Chart, buffer: &mut Vec<u32>, width: usize, height: usize) {
    for i in buffer.iter_mut() {
        *i = 0;
    }
    let iwidth = width as isize;
    let iheight = height as isize;
    let set_pixel = |buf: &mut Vec<u32>, (x, y): Coord, clr| {
        if x >= 0 && x < iwidth && y >= 0 && y < iheight {
            buf[y as usize * width + x as usize] = clr;
        }
    };

    let viewport = (2, 2);

    for coord in chart.bbox.iter() {
        let rel = relative_coord(coord, chart.bbox.top_left());
        if let Some(color) = chart.stage.get(rel).map(|cell| match cell {
            Acre::Open => 0x101010u32,
            Acre::Wooded => 0x27A027u32,
            Acre::Lumberyard => 0x406020u32,
        }) {
            let rel = offset_coord(rel, viewport);
            set_pixel(buffer, rel, color);
        }
    }
}

use std::sync::mpsc;

fn main() {
    let (tx, rx) = mpsc::channel();
    std::thread::spawn(move || {
        run_problem(std::fs::File::open("input.txt").unwrap(), |chart| {
            tx.send(chart.clone()).unwrap()
        });
    });

    const WIDTH: usize = 55;
    const HEIGHT: usize = 55;
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut window = Window::new(
        "Test - ESC to exit",
        WIDTH,
        HEIGHT,
        WindowOptions {
            borderless: false,
            title: true,
            resize: false,
            scale: Scale::X16,
        },
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    let mut history = Vec::new();
    let mut shown_history: Option<usize> = None;
    let mut wanted_history: Option<usize> = None;
    let mut follow = true;

    while window.is_open() && !window.is_key_down(Key::Escape) {
        if window.is_key_pressed(Key::Right, KeyRepeat::Yes) && !history.is_empty() {
            wanted_history = wanted_history
                .map(|cur| (cur + 1).min(history.len() - 1))
                .or(Some(0));
        }

        if window.is_key_pressed(Key::Left, KeyRepeat::Yes) && !history.is_empty() {
            follow = false;
            wanted_history = wanted_history.map(|cur| cur.saturating_sub(1)).or(Some(0));
        }

        if window.is_key_pressed(Key::Home, KeyRepeat::No) && !history.is_empty() {
            follow = false;
            wanted_history = Some(0);
        }

        if window.is_key_pressed(Key::End, KeyRepeat::No) && !history.is_empty() {
            follow = false;
            wanted_history = Some(history.len() - 1);
        }

        if window.is_key_pressed(Key::F, KeyRepeat::No) && !history.is_empty() {
            follow = !follow;
        }

        if let Ok(chart) = rx.try_recv() {
            history.push(chart);
            if wanted_history.is_none() {
                wanted_history = Some(0);
            }
        }

        if follow {
            if !history.is_empty() {
                wanted_history = Some(history.len() - 1);
            }
        }

        if shown_history != wanted_history {
            render_chart(
                &history[wanted_history.unwrap()],
                &mut buffer,
                WIDTH,
                HEIGHT,
            );
            shown_history = wanted_history;
        }

        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window.update_with_buffer(&buffer).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn given() {
        let res = run_problem(
            std::io::Cursor::new(
                r#".#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|."#,
            ),
            |chart| {
                chart.bbox.iter().for_each(|c| {
                    eprint!(
                        "{}",
                        match chart.stage.get(c).unwrap() {
                            Acre::Open => '.',
                            Acre::Wooded => '|',
                            Acre::Lumberyard => '#',
                        }
                    );
                    if c.0 == chart.stage.width as isize - 1 {
                        eprintln!("");
                    }
                });
                eprintln!("");
            },
        );
        assert_eq!(res, 1147);
    }
}
