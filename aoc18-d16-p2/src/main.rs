use std::collections::HashSet;
use std::io::{BufRead, BufReader, Read};
use std::iter::FromIterator;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Registers {
    r: [u64; 4],
}

impl Registers {
    fn new() -> Self {
        Registers { r: [0u64; 4] }
    }

    fn parse(s: &str) -> Option<Self> {
        let mut it = s.matches(char::is_numeric);
        Some(Registers {
            r: [
                it.next().unwrap().parse().unwrap(),
                it.next().unwrap().parse().unwrap(),
                it.next().unwrap().parse().unwrap(),
                it.next().unwrap().parse().unwrap(),
            ],
        })
    }
}

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
enum Op {
    AddR,
    AddI,
    MulR,
    MulI,
    BanR,
    BanI,
    BorR,
    BorI,
    SetR,
    SetI,
    GTIR,
    GTRI,
    GTRR,
    EQIR,
    EQRI,
    EQRR,
}

impl Op {
    fn eval(&self, a: u64, b: u64, c: u64, mut reg: Registers) -> Registers {
        let aidx = a as usize;
        let bidx = b as usize;
        let cidx = c as usize;
        reg.r[cidx] = match self {
            Op::AddR => reg.r[aidx] + reg.r[bidx],
            Op::AddI => reg.r[aidx] + b,
            Op::MulR => reg.r[aidx] * reg.r[bidx],
            Op::MulI => reg.r[aidx] * b,
            Op::BanR => reg.r[aidx] & reg.r[bidx],
            Op::BanI => reg.r[aidx] & b,
            Op::BorR => reg.r[aidx] | reg.r[bidx],
            Op::BorI => reg.r[aidx] | b,
            Op::SetR => reg.r[aidx],
            Op::SetI => a,
            Op::GTIR => {
                if a > reg.r[bidx] {
                    1
                } else {
                    0
                }
            }
            Op::GTRI => {
                if reg.r[aidx] > b {
                    1
                } else {
                    0
                }
            }
            Op::GTRR => {
                if reg.r[aidx] > reg.r[bidx] {
                    1
                } else {
                    0
                }
            }
            Op::EQIR => {
                if a == reg.r[bidx] {
                    1
                } else {
                    0
                }
            }
            Op::EQRI => {
                if reg.r[aidx] == b {
                    1
                } else {
                    0
                }
            }
            Op::EQRR => {
                if reg.r[aidx] == reg.r[bidx] {
                    1
                } else {
                    0
                }
            }
        };
        reg
    }
}

struct VM {}

#[derive(Debug, Clone, Copy)]
struct Instruction {
    op: u8,
    a: u64,
    b: u64,
    c: u64,
}

impl Instruction {
    fn parse(s: &str) -> Option<Self> {
        let mut it = s.split(" ");
        Some(Instruction {
            op: it.next().unwrap().parse().unwrap(),
            a: it.next().unwrap().parse().unwrap(),
            b: it.next().unwrap().parse().unwrap(),
            c: it.next().unwrap().parse().unwrap(),
        })
    }
}

#[derive(Debug, Clone, Copy)]
struct Sample {
    before: Registers,
    instruction: Instruction,
    after: Registers,
}

fn run_problem<F: Read>(samples_fh: F, program_fh: F) {
    let samples = BufReader::new(samples_fh)
        .lines()
        .map(|line| line.unwrap())
        .collect::<Vec<String>>()
        .chunks_exact(4)
        .map(|chunk| Sample {
            before: Registers::parse(chunk[0].split(": ").nth(1).unwrap()).unwrap(),
            instruction: Instruction::parse(&chunk[1]).unwrap(),
            after: Registers::parse(chunk[2].split(": ").nth(1).unwrap()).unwrap(),
        })
        .collect::<Vec<Sample>>();

    let full_set: HashSet<Op> = [
        Op::AddR,
        Op::AddI,
        Op::MulR,
        Op::MulI,
        Op::BanR,
        Op::BanI,
        Op::BorR,
        Op::BorI,
        Op::SetR,
        Op::SetI,
        Op::GTIR,
        Op::GTRI,
        Op::GTRR,
        Op::EQIR,
        Op::EQRI,
        Op::EQRR,
    ]
    .into_iter()
    .cloned()
    .collect();
    let mut candidates = std::iter::repeat(&full_set)
        .take(16)
        .cloned()
        .collect::<Vec<HashSet<_>>>();

    for sample in &samples {
        let Instruction { op: id, a, b, c } = sample.instruction;
        candidates[id as usize].retain(|op| op.eval(a, b, c, sample.before) == sample.after);
    }

    while candidates.iter().any(|cand| cand.len() > 1) {
        for i in 0..16 {
            if candidates[i].len() == 1 {
                let op = candidates[i].iter().next().unwrap().clone();
                for j in 0..16 {
                    if i != j {
                        candidates[j].remove(&op);
                    }
                }
            }
        }
    }
    let op_map = candidates
        .iter()
        .map(|cand| cand.iter().next().unwrap())
        .cloned()
        .collect::<Vec<Op>>();

    let result = BufReader::new(program_fh)
        .lines()
        .map(|line| Instruction::parse(&line.unwrap()).unwrap())
        .fold(Registers::new(), |mut reg, insn| {
            let op = op_map[insn.op as usize];
            op.eval(insn.a, insn.b, insn.c, reg)
        });
        eprintln!("Final registers are {:?}", result.r);
}

fn main() {
    run_problem(
        std::fs::File::open("samples.txt").unwrap(),
        std::fs::File::open("program.txt").unwrap(),
    );
    //     run_problem(std::io::Cursor::new(
    //         r#"Before: [3, 2, 1, 1]
    // 9 2 1 2
    // After:  [3, 2, 2, 1]

    // "#,
    //     ));
}
