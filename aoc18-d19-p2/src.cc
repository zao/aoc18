#include <stdint.h>
#include <stdio.h>

void f(int r[6]) {
    int& r0 = r[0];
    int& r1 = r[1];
    int& ip = r[2];
    int& r3 = r[3];
    int& r4 = r[4];
    int& r5 = r[5];
    i0: ip = ip + 16;  // goto init;
    i1: r1 = 1;
    i2: r4 = 1;
    i3: r3 = r1 * r4;
    i4: r3 = r3 == r5; // branch
    i5: ip += r3;      // jump forward
    i6: ip += 1;       // relative jump
    i7: r0 += r1;
    i8: r4 += 1;
    i9: r3 = r4 > r5;  // branch
    i10: ip += r3;     // jump forward
    i11: ip = 2;       // absolute jump
    i12: r1 += 1;
    i13: r3 = r1 > r5; // 
    i14: ip += r3;     // if (!(r1 > r5)) {
    i15: ip = 1;       //   goto i1;
                       // } else {
    i16: ip *= ip;     //   goto end; // (jump to 256)
                       // }
    init:
    i17: r5 += 2;
    i18: r5 *= r5;
    i19: r5 *= ip;
    i20: r5 *= 11;
    i21: r3 += 4;
    i22: r3 *= ip;
    i23: r3 += 7;
    i24: r5 += r3;
    i25: ip += r0; // jump forward
    i26: ip = 0; // absolute jump
    i27: r3 = ip;
    i28: r3 += ip;
    i29: r3 += ip;
    i30: r3 *= ip;
    i31: r3 *= 14;
    i32: r3 *= ip;
    i33: r5 += r3;
    i34: r0 = 0;
    i35: ip = 0; // absolute jump
    i36to256:
    end:
}

void run() {
    int regs[6] = { 1 };
    f(regs);
}

int main() {
    uint64_t i = 0;
    int r[6] = { 1, 0, 0, 0, 0, 0};
    int* ipr = &r[2];
    while (*ipr < 36) {
        switch (*ipr) {
            case 0: r[2] = r[2] + 16; break; // addi 2 16 2
            case 1: r[1] = 1; break; // seti 1 4 1
            case 2: r[4] = 1; break; // seti 1 2 4
            case 3: r[3] = r[1] * r[4]; break; // mulr 1 4 3
            case 4: r[3] = r[3] == r[5]; break; // eqrr 3 5 3
            case 5: r[2] = r[3] + r[2]; break; // addr 3 2 2
            case 6: r[2] = r[2] + 1; break; // addi 2 1 2
            case 7: r[0] = r[1] + r[0]; break; // addr 1 0 0
            case 8: r[4] = r[4] + 1; break; // addi 4 1 4
            case 9: r[3] = r[4] > r[5]; break; // gtrr 4 5 3
            case 10: r[2] = r[2] + r[3]; break; // addr 2 3 2
            case 11: r[2] = 2; break; // seti 2 7 2
            case 12: r[1] = r[1] + 1; break; // addi 1 1 1
            case 13: r[3] = r[1] > r[5]; break; // gtrr 1 5 3
            case 14: r[2] = r[3] + r[2]; break; // addr 3 2 2
            case 15: r[2] = 1; break; // seti 1 0 2
            case 16: r[2] = r[2] * r[2]; break; // mulr 2 2 2
            case 17: r[5] = r[5] + 2; break; // addi 5 2 5
            case 18: r[5] = r[5] * r[5]; break; // mulr 5 5 5
            case 19: r[5] = r[2] * r[5]; break; // mulr 2 5 5
            case 20: r[5] = r[5] * 11; break; // muli 5 11 5
            case 21: r[3] = r[3] + 4; break; // addi 3 4 3
            case 22: r[3] = r[3] * r[2]; break; // mulr 3 2 3
            case 23: r[3] = r[3] + 7; break; // addi 3 7 3
            case 24: r[5] = r[5] + r[3]; break; // addr 5 3 5
            case 25: r[2] = r[2] + r[0]; break; // addr 2 0 2
            case 26: r[2] = 0; break; // seti 0 1 2
            case 27: r[3] = r[2]; break; // setr 2 1 3
            case 28: r[3] = r[3] * r[2]; break; // mulr 3 2 3
            case 29: r[3] = r[2] + r[3]; break; // addr 2 3 3
            case 30: r[3] = r[2] * r[3]; break; // mulr 2 3 3
            case 31: r[3] = r[3] * 14; break; // muli 3 14 3
            case 32: r[3] = r[3] * r[2]; break; // mulr 3 2 3
            case 33: r[5] = r[5] + r[3]; break; // addr 5 3 5
            case 34: r[0] = 0; break; // seti 0 9 0
            case 35: r[2] = 0; break; // seti 0 8 2
        }
        ++*ipr;
        if (i++ % 10000000 == 0) {
            printf("ip=%d [%d %d %d %d %d %d]\n", *ipr, r[0], r[1], r[2], r[3], r[4], r[5]);
        }
    }
    printf("ip=%d [%d %d %d %d %d %d]\n", *ipr, r[0], r[1], r[2], r[3], r[4], r[5]);
    printf("%d", r[0]);
    return 0;
}