use std::io::{BufRead, BufReader, Read};
use std::str::FromStr;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Registers {
    r: [u64; 6],
}

impl Registers {
    fn new() -> Self {
        Registers { r: [0u64; 6] }
    }
}

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
enum Op {
    AddR,
    AddI,
    MulR,
    MulI,
    BanR,
    BanI,
    BorR,
    BorI,
    SetR,
    SetI,
    GTIR,
    GTRI,
    GTRR,
    EQIR,
    EQRI,
    EQRR,
}

impl Op {
    fn eval(&self, a: u64, b: u64, c: u64, mut reg: Registers) -> Registers {
        let aidx = a as usize;
        let bidx = b as usize;
        let cidx = c as usize;
        reg.r[cidx] = match self {
            Op::AddR => reg.r[aidx] + reg.r[bidx],
            Op::AddI => reg.r[aidx] + b,
            Op::MulR => reg.r[aidx] * reg.r[bidx],
            Op::MulI => reg.r[aidx] * b,
            Op::BanR => reg.r[aidx] & reg.r[bidx],
            Op::BanI => reg.r[aidx] & b,
            Op::BorR => reg.r[aidx] | reg.r[bidx],
            Op::BorI => reg.r[aidx] | b,
            Op::SetR => reg.r[aidx],
            Op::SetI => a,
            Op::GTIR => (a > reg.r[bidx]) as u64,
            Op::GTRI => (reg.r[aidx] > b) as u64,
            Op::GTRR => (reg.r[aidx] > reg.r[bidx]) as u64,
            Op::EQIR => (a == reg.r[bidx]) as u64,
            Op::EQRI => (reg.r[aidx] == b) as u64,
            Op::EQRR => (reg.r[aidx] == reg.r[bidx]) as u64,
        };
        reg
    }
}

impl FromStr for Op {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "addr" => Ok(Op::AddR),
            "addi" => Ok(Op::AddI),
            "mulr" => Ok(Op::MulR),
            "muli" => Ok(Op::MulI),
            "banr" => Ok(Op::BanR),
            "bani" => Ok(Op::BanI),
            "borr" => Ok(Op::BorR),
            "bori" => Ok(Op::BorI),
            "setr" => Ok(Op::SetR),
            "seti" => Ok(Op::SetI),
            "gtir" => Ok(Op::GTIR),
            "gtri" => Ok(Op::GTRI),
            "gtrr" => Ok(Op::GTRR),
            "eqir" => Ok(Op::EQIR),
            "eqri" => Ok(Op::EQRI),
            "eqrr" => Ok(Op::EQRR),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Instruction {
    op: Op,
    a: u64,
    b: u64,
    c: u64,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.split(" ");
        Ok(Instruction {
            op: it.next().unwrap().parse().unwrap(),
            a: it.next().unwrap().parse().unwrap(),
            b: it.next().unwrap().parse().unwrap(),
            c: it.next().unwrap().parse().unwrap(),
        })
    }
}

struct VM {
    program: Vec<Instruction>,
    regs: Registers,
    ip_reg: usize,
}

impl VM {
    fn dump_c(&self) {
        macro_rules! fmt_insn {
            ( $insn:expr, $x:expr ) => { format!(concat!($x, " {a} {b} {c}"), a = $insn.a, b = $insn.b, c = $insn.c) };
        }
        for (ip, insn) in self.program.iter().enumerate() {
            let rep = match insn.op {
                Op::AddR => fmt_insn!(insn, "r[{c}] = r[{a}] + r[{b}]; break; // addr"),
                Op::AddI => fmt_insn!(insn, "r[{c}] = r[{a}] + {b}; break; // addi"),
                Op::MulR => fmt_insn!(insn, "r[{c}] = r[{a}] * r[{b}]; break; // mulr"),
                Op::MulI => fmt_insn!(insn, "r[{c}] = r[{a}] * {b}; break; // muli"),
                Op::BanR => fmt_insn!(insn, "r[{c}] = r[{a}] & r[{b}]; break; // banr"),
                Op::BanI => fmt_insn!(insn, "r[{c}] = r[{a}] & {b}; break; // bani"),
                Op::BorR => fmt_insn!(insn, "r[{c}] = r[{a}] | r[{b}]; break; // borr"),
                Op::BorI => fmt_insn!(insn, "r[{c}] = r[{a}] | {b}; break; // bori"),
                Op::SetR => fmt_insn!(insn, "r[{c}] = r[{a}]; break; // setr"),
                Op::SetI => fmt_insn!(insn, "r[{c}] = {a}; break; // seti"),
                Op::GTIR => fmt_insn!(insn, "r[{c}] = {a} > r[{b}]; break; // gtir"),
                Op::GTRI => fmt_insn!(insn, "r[{c}] = r[{a}] > {b}; break; // gtri"),
                Op::GTRR => fmt_insn!(insn, "r[{c}] = r[{a}] > r[{b}]; break; // gtrr"),
                Op::EQIR => fmt_insn!(insn, "r[{c}] = {a} == r[{b}]; break; // eqir"),
                Op::EQRI => fmt_insn!(insn, "r[{c}] = r[{a}] == {b}; break; // eqri"),
                Op::EQRR => fmt_insn!(insn, "r[{c}] = r[{a}] == r[{b}]; break; // eqrr"),
            };
            eprintln!("case {}: {}", ip, rep);
        }
    }
}

fn run_problem<F: Read>(program_fh: F, start_regs: &[u64; 6]) -> u64 {
    let mut ip_reg = 0;
    let program: Vec<Instruction> = BufReader::new(program_fh)
        .lines()
        .filter_map(|line| {
            let line = line.unwrap();
            if line.chars().nth(0).unwrap() == '#' {
                ip_reg = line.split(' ').nth(1).unwrap().parse().unwrap();
                None
            } else {
                Some(line.parse().unwrap())
            }
        })
        .collect();
    let mut vm = VM {
        program,
        regs: Registers::new(),
        ip_reg,
    };
    vm.regs.r = *start_regs;
    // vm.dump_c();
    // panic!();
    while vm.regs.r[ip_reg] < vm.program.len() as u64 {
        let ip = vm.regs.r[vm.ip_reg] as usize;
        let insn = vm.program[ip];
        eprint!("ip={:>2} ", ip);
        eprint!(
            "[{:>8}, {:>8}, {:>8}, {:>8}, {:>8}, {:>8}] ",
            vm.regs.r[0], vm.regs.r[1], vm.regs.r[2], vm.regs.r[3], vm.regs.r[4], vm.regs.r[5]
        );
        eprint!("{:?} {:>2} {:>2} {:>2} ", insn.op, insn.a, insn.b, insn.c);
        vm.regs = insn.op.eval(insn.a, insn.b, insn.c, vm.regs);
        eprintln!(
            "[{:>8}, {:>8}, {:>8}, {:>8}, {:>8}, {:>8}]",
            vm.regs.r[0], vm.regs.r[1], vm.regs.r[2], vm.regs.r[3], vm.regs.r[4], vm.regs.r[5]
        );
        vm.regs.r[vm.ip_reg] += 1;
    }
    eprintln!("Register 0 at halt is {}", vm.regs.r[0]);
    vm.regs.r[0]
}

fn main() {
    // run_problem(std::fs::File::open("program.txt").unwrap(), &[1, 0, 0, 0, 0, 0]);
    run_problem(
        std::fs::File::open("program.txt").unwrap(),
        &[1, 0, 0, 0, 0, 0],
        // &[0, 1, 9, 0, 10551330, 10551331],
        // &[1, 2, 9, 0, 10551330, 10551331],
        // &[1, 2, 9, 0, 10551330, 10551331],
    );
}
