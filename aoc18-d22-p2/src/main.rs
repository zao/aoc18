use std::collections::HashMap;
use std::sync::mpsc;
use std::thread;

use binary_heap_plus::*;

type Coord = (isize, isize);

#[derive(Debug)]
struct Cell {
    erosion_level: usize,
}

#[derive(Debug, Clone, Copy)]
enum RegionType {
    Rocky,
    Wet,
    Narrow,
}

impl RegionType {
    fn from_erosion_level(level: usize) -> RegionType {
        match level % 3 {
            0 => RegionType::Rocky,
            1 => RegionType::Wet,
            2 => RegionType::Narrow,
            _ => unreachable!(),
        }
    }

    fn risk_level(self) -> usize {
        match self {
            RegionType::Rocky => 0,
            RegionType::Wet => 1,
            RegionType::Narrow => 2,
        }
    }

    fn glyph(self) -> char {
        match self {
            RegionType::Rocky => '.',
            RegionType::Wet => '=',
            RegionType::Narrow => '|',
        }
    }
}

type TerrainQuery = Coord;
type TerrainAnswer = Option<RegionType>;

#[derive(Debug)]
struct Chart {
    levels: Vec<Vec<usize>>,
    depth: usize,
}

impl Chart {
    fn new(depth: usize, target: Coord) -> Chart {
        let erosion_level = |gi| (gi + depth) % 20183;
        let cx = target.0 as usize;
        let cy = target.1 as usize;
        let mut levels: Vec<Vec<usize>> =
            vec![(0..=cx).map(|x| erosion_level(x * 16807)).collect()];
        for y in 1..=cy {
            let mut row = vec![erosion_level(y * 48271)];
            row.reserve_exact(cx - 1);
            for x in 1..=cx {
                let above = levels[y - 1][x];
                let left = row[x - 1];
                let level = erosion_level(above * left);
                row.push(level);
            }
            levels.push(row);
        }
        levels[target.1 as usize][target.0 as usize] = erosion_level(0);
        Chart { levels, depth }
    }

    fn expand_horizontally(&mut self, desired_x: usize) {
        let depth = self.depth;
        let erosion_level = |gi| (gi + depth) % 20183;
        let current_width = self.levels[0].len();
        if desired_x >= current_width {
            for x in current_width..=desired_x {
                self.levels[0].push(erosion_level(x * 16807));
            }
            for y in 1..self.levels.len() {
                for x in current_width..=desired_x {
                    let above = self.levels[y - 1][x];
                    let left = self.levels[y][x - 1];
                    self.levels[y].push(erosion_level(above * left));
                }
            }
        }
    }

    fn expand_vertically(&mut self, desired_y: usize) {
        let depth = self.depth;
        let erosion_level = |gi| (gi + depth) % 20183;
        if desired_y >= self.levels.len() {
            for y in self.levels.len()..=desired_y {
                let mut row = vec![erosion_level(y * 48271)];
                for x in 1..self.levels[0].len() {
                    let above = self.levels[y - 1][x];
                    let left = row[x - 1];
                    row.push(erosion_level(above * left));
                }
                self.levels.push(row);
            }
        }
    }

    fn expand(&mut self, cx: usize, cy: usize) -> RegionType {
        self.expand_horizontally(cx);
        self.expand_vertically(cy);
        RegionType::from_erosion_level(self.levels[cy][cx])
    }
}

#[derive(Debug)]
struct Oracle {
    // cells: Vec<Vec<Cell>>,
    // depth: usize,
    target: Coord,
    query_tx: mpsc::Sender<TerrainQuery>,
    answer_rx: mpsc::Receiver<TerrainAnswer>,
}

impl Oracle {
    fn new(depth: usize, target: Coord) -> Oracle {
        let (query_tx, query_rx) = mpsc::channel::<TerrainQuery>();
        let (answer_tx, answer_rx) = mpsc::channel::<TerrainAnswer>();
        thread::spawn(move || {
            let mut chart = Chart::new(depth, target);
            let mut cells = Vec::new();
            cells.push(vec![Cell {
                erosion_level: depth % 20183,
            }]);

            while let Ok(query) = query_rx.recv() {
                let cx = query.0 as usize;
                let cy = query.1 as usize;
                let region_type = if query.0 < 0 || query.1 < 0 {
                    None
                } else {
                    Some(chart.expand(cx, cy))
                };
                answer_tx.send(region_type).unwrap();
            }
        });
        Oracle {
            target,
            query_tx,
            answer_rx,
        }
    }

    fn region_type(&self, coord: Coord) -> Option<RegionType> {
        self.query_tx.send(coord).unwrap();
        self.answer_rx.recv().unwrap()
    }

    // fn erosion_level(&self, coord: Coord) -> usize {
    //     let gi: usize = match coord {
    //         (0, 0) => 0,
    //         _ if coord == self.target => 0 as usize,
    //         (x, 0) => x as usize * 16807,
    //         (0, y) => y as usize * 48271,
    //         (x, y) => self.erosion_level((x - 1, y)) * self.erosion_level((x, y - 1)),
    //     };
    //     let el = (gi + self.depth) % 20183;
    //     el
    // }

    fn print(&self, top_left: Coord, bottom_right: Coord) {
        for y in top_left.1..=bottom_right.1 {
            for x in top_left.0..=bottom_right.0 {
                let coord = (x, y);
                let glyph = match coord {
                    (0, 0) => 'M',
                    coord if coord == self.target => 'T',
                    _ => self.region_type(coord).unwrap().glyph(),
                };
                eprint!("{}", glyph);
            }
            eprintln!("");
        }
    }
}

#[derive(Debug, Clone, Copy, Ord, PartialOrd, Eq, PartialEq, Hash)]
enum Tool {
    Torch,
    ClimbingGear,
    Neither,
}

#[derive(Debug, Clone, Copy)]
enum Direction {
    North,
    West,
    East,
    South,
}

fn neighbour_coord(coord: Coord, dir: Direction) -> Coord {
    match dir {
        Direction::North => (coord.0, coord.1 - 1),
        Direction::West => (coord.0 - 1, coord.1),
        Direction::East => (coord.0 + 1, coord.1),
        Direction::South => (coord.0, coord.1 + 1),
    }
}

fn adjacent4(coord: Coord) -> [Coord; 4] {
    [
        neighbour_coord(coord, Direction::North),
        neighbour_coord(coord, Direction::West),
        neighbour_coord(coord, Direction::East),
        neighbour_coord(coord, Direction::South),
    ]
}

fn mount_rescue(oracle: &Oracle) -> usize {
    let mut wavefront = BinaryHeap::new_min();
    wavefront.push((0, (0, 0), Tool::Torch));

    let mut visited = HashMap::new();

    while let Some((time, coord, tool)) = wavefront.pop() {
        let memo = (coord, tool);
        // Skip if we have been here with this tool earlier.
        if let Some(prev_time) = visited.get(&memo) {
            if *prev_time <= time {
                continue;
            }
        }

        // eprintln!("visiting {:?} at {}", memo, time);
        visited.insert(memo, time);

        // Complete if we've found the target with the right tool.
        if coord == oracle.target && tool == Tool::Torch {
            return time;
        }

        let current_type = oracle.region_type(coord).unwrap();
        wavefront.push((
            time + 7,
            coord,
            match (current_type, tool) {
                (RegionType::Rocky, Tool::ClimbingGear) => Tool::Torch,
                (RegionType::Rocky, Tool::Torch) => Tool::ClimbingGear,
                (RegionType::Wet, Tool::ClimbingGear) => Tool::Neither,
                (RegionType::Wet, Tool::Neither) => Tool::ClimbingGear,
                (RegionType::Narrow, Tool::Torch) => Tool::Neither,
                (RegionType::Narrow, Tool::Neither) => Tool::Torch,
                _ => unreachable!(),
            },
        ));
        for coord in adjacent4(coord).iter() {
            if let Some(adj_type) = oracle.region_type(*coord) {
                if match (adj_type, tool) {
                    (RegionType::Rocky, Tool::ClimbingGear) => true,
                    (RegionType::Rocky, Tool::Torch) => true,
                    (RegionType::Wet, Tool::ClimbingGear) => true,
                    (RegionType::Wet, Tool::Neither) => true,
                    (RegionType::Narrow, Tool::Torch) => true,
                    (RegionType::Narrow, Tool::Neither) => true,
                    _ => false,
                } {
                    wavefront.push((time + 1, *coord, tool));
                }
            }
        }
    }

    panic!();
}

fn run_problem(depth: usize, target: Coord) -> usize {
    let oracle = Oracle::new(depth, target);

    let time_taken = mount_rescue(&oracle);
    println!("Time taken: {}", time_taken);

    9001
}

fn main() {
    run_problem(11820, (7, 782));
    // run_problem(510, (10, 10));
}
