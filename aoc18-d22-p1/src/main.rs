use std::collections::HashMap;

type Coord = (isize, isize);

#[derive(Debug)]
struct Cell {
    erosion_level: usize,
}

#[derive(Debug, Clone, Copy)]
enum RegionType {
    Rocky,
    Wet,
    Narrow,
}

impl RegionType {
    fn from_erosion_level(level: usize) -> RegionType {
        match level % 3 {
            0 => RegionType::Rocky,
            1 => RegionType::Wet,
            2 => RegionType::Narrow,
            _ => unreachable!(),
        }
    }

    fn risk_level(self) -> usize {
        match self {
            RegionType::Rocky => 0,
            RegionType::Wet => 1,
            RegionType::Narrow => 2,
        }
    }

    fn glyph(self) -> char {
        match self {
            RegionType::Rocky => '.',
            RegionType::Wet => '=',
            RegionType::Narrow => '|',
        }
    }
}

#[derive(Debug)]
struct Chart {
    cells: HashMap<Coord, Cell>,
    depth: usize,
    target: Coord,
}

impl Chart {
    fn new(depth: usize, target: Coord) -> Chart {
        Chart {
            cells: HashMap::new(),
            depth,
            target,
        }
    }

    fn erosion_level(&mut self, coord: Coord) -> usize {
        if let Some(cell) = self.cells.get(&coord) {
            return cell.erosion_level;
        }
        let gi: usize = match coord {
            (0, 0) => 0,
            _ if coord == self.target => 0 as usize,
            (x, 0) => x as usize * 16807,
            (0, y) => y as usize * 48271,
            (x, y) => self.erosion_level((x - 1, y)) * self.erosion_level((x, y - 1)),
        };
        let el = (gi + self.depth) % 20183;
        self.cells.insert(coord, Cell { erosion_level: el });
        el
    }

    fn print(&self, top_left: Coord, bottom_right: Coord) {
        for y in top_left.1..=bottom_right.1 {
            for x in top_left.0..=bottom_right.0 {
                let coord = (x, y);
                let glyph = if coord == top_left {
                    'M'
                } else {
                    let cell = self.cells.get(&coord).unwrap();
                    let region_type = RegionType::from_erosion_level(cell.erosion_level);
                    region_type.glyph()
                };
                eprint!("{}", glyph);
            }
            eprintln!("");
        }
    }
}

fn run_problem(depth: usize, target: Coord) -> usize {
    let mut chart = Chart::new(depth, target);
    let mut risk_level = 0;
    for y in 0..=target.1 {
        for x in 0..=target.0 {
            let erosion_level = chart.erosion_level((x, y));
            risk_level += RegionType::from_erosion_level(erosion_level).risk_level();
        }
    }
    chart.print((0, 0), target);
    eprintln!("Risk level: {}", risk_level);

    9001
}

fn main() {
    run_problem(11820, (7, 782));
    // run_problem(510, (10, 10));
}
