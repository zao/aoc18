use std::collections::HashSet;
use std::io::{BufRead, BufReader, Read};

#[derive(Debug, Clone, Copy)]
struct Coord {
    x: isize,
    y: isize,
    z: isize,
    w: isize,
}

fn manhattan_distance(a: Coord, b: Coord) -> isize {
    (a.x - b.x).abs() + (a.y - b.y).abs() + (a.z - b.z).abs() + (a.w - b.w).abs()
}

type ConstellationId = usize;

fn run_problem<F: Read>(fh: F) -> usize {
    let stars: Vec<Coord> = BufReader::new(fh)
        .lines()
        .map(|line| {
            let line = line.unwrap();
            let s = line.trim();
            let mut parts = s.split(",");
            Coord {
                x: parts.next().unwrap().parse().unwrap(),
                y: parts.next().unwrap().parse().unwrap(),
                z: parts.next().unwrap().parse().unwrap(),
                w: parts.next().unwrap().parse().unwrap(),
            }
        })
        .collect();

    let mut constellation_map: Vec<ConstellationId> = (0..stars.len()).collect();

    for star_id in 0..stars.len() {
        for cand_id in star_id + 1..stars.len() {
            if manhattan_distance(stars[star_id], stars[cand_id]) <= 3 {
                let a = constellation_map[star_id];
                let b = constellation_map[cand_id];
                if a != b {
                    for m in &mut constellation_map {
                        if *m == b {
                            *m = a;
                        }
                    }
                }
            }
        }
    }

    let constellation_tally: HashSet<ConstellationId> = constellation_map.iter().cloned().collect();

    let count = constellation_tally.len();
    eprintln!("Constellation count: {}", count);

    count
}

fn main() {
    if std::env::args().count() > 1 {
        let samples = vec![
            (
                r#" 0,0,0,0
3,0,0,0
0,3,0,0
0,0,3,0
0,0,0,3
0,0,0,6
9,0,0,0
12,0,0,0"#,
                2,
            ),
            (
                r#"-1,2,2,0
0,0,2,-2
0,0,0,-2
-1,2,0,0
-2,-2,-2,2
3,0,2,-1
-1,3,2,2
-1,0,-1,0
0,2,1,-2
3,0,0,0"#,
                4,
            ),
            (
                r#"1,-1,0,1
2,0,-1,0
3,2,-1,0
0,0,3,1
0,0,-1,-1
2,3,-2,0
-2,2,0,0
2,-2,0,-1
1,-1,0,-1
3,2,0,2"#,
                3,
            ),
            (
                r#"1,-1,-1,-2
-2,-2,0,1
0,2,1,3
-2,3,-2,1
0,2,3,-2
-1,-1,1,-2
0,-2,-1,0
-2,2,3,-1
1,2,2,0
-1,-2,0,-2"#,
                8,
            ),
        ];
        for sample in samples {
            assert_eq!(run_problem(std::io::Cursor::new(sample.0)), sample.1);
        }
    } else {
        run_problem(std::fs::File::open("input.txt").unwrap());
    }
}
