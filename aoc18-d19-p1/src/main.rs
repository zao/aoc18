use std::io::{BufRead, BufReader, Read};
use std::str::FromStr;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Registers {
    r: [u64; 6],
}

impl Registers {
    fn new() -> Self {
        Registers { r: [0u64; 6] }
    }
}

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
enum Op {
    AddR,
    AddI,
    MulR,
    MulI,
    BanR,
    BanI,
    BorR,
    BorI,
    SetR,
    SetI,
    GTIR,
    GTRI,
    GTRR,
    EQIR,
    EQRI,
    EQRR,
}

impl Op {
    fn eval(&self, a: u64, b: u64, c: u64, mut reg: Registers) -> Registers {
        let aidx = a as usize;
        let bidx = b as usize;
        let cidx = c as usize;
        reg.r[cidx] = match self {
            Op::AddR => reg.r[aidx] + reg.r[bidx],
            Op::AddI => reg.r[aidx] + b,
            Op::MulR => reg.r[aidx] * reg.r[bidx],
            Op::MulI => reg.r[aidx] * b,
            Op::BanR => reg.r[aidx] & reg.r[bidx],
            Op::BanI => reg.r[aidx] & b,
            Op::BorR => reg.r[aidx] | reg.r[bidx],
            Op::BorI => reg.r[aidx] | b,
            Op::SetR => reg.r[aidx],
            Op::SetI => a,
            Op::GTIR => (a > reg.r[bidx]) as u64,
            Op::GTRI => (reg.r[aidx] > b) as u64,
            Op::GTRR => (reg.r[aidx] > reg.r[bidx]) as u64,
            Op::EQIR => (a == reg.r[bidx]) as u64,
            Op::EQRI => (reg.r[aidx] == b) as u64,
            Op::EQRR => (reg.r[aidx] == reg.r[bidx]) as u64,
        };
        reg
    }
}

impl FromStr for Op {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "addr" => Ok(Op::AddR),
            "addi" => Ok(Op::AddI),
            "mulr" => Ok(Op::MulR),
            "muli" => Ok(Op::MulI),
            "banr" => Ok(Op::BanR),
            "bani" => Ok(Op::BanI),
            "borr" => Ok(Op::BorR),
            "bori" => Ok(Op::BorI),
            "setr" => Ok(Op::SetR),
            "seti" => Ok(Op::SetI),
            "gtir" => Ok(Op::GTIR),
            "gtri" => Ok(Op::GTRI),
            "gtrr" => Ok(Op::GTRR),
            "eqir" => Ok(Op::EQIR),
            "eqri" => Ok(Op::EQRI),
            "eqrr" => Ok(Op::EQRR),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Instruction {
    op: Op,
    a: u64,
    b: u64,
    c: u64,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.split(" ");
        Ok(Instruction {
            op: it.next().unwrap().parse().unwrap(),
            a: it.next().unwrap().parse().unwrap(),
            b: it.next().unwrap().parse().unwrap(),
            c: it.next().unwrap().parse().unwrap(),
        })
    }
}

struct VM {
    program: Vec<Instruction>,
    regs: Registers,
    ip_reg: usize,
}

fn run_problem<F: Read>(program_fh: F) -> u64 {
    let mut ip_reg = 0;
    let program: Vec<Instruction> = BufReader::new(program_fh)
        .lines()
        .filter_map(|line| {
            let line = line.unwrap();
            if line.chars().nth(0).unwrap() == '#' {
                ip_reg = line.split(' ').nth(1).unwrap().parse().unwrap();
                None
            }
            else {
                Some(line.parse().unwrap())
            }
        }).collect();
    let mut vm = VM {
        program,
        regs: Registers::new(),
        ip_reg,
    };
    while vm.regs.r[ip_reg] < vm.program.len() as u64 {
        let ip = vm.regs.r[vm.ip_reg] as usize;
        let insn = vm.program[ip];
        // eprint!("ip={} {:?} {:?}", ip, vm.regs, insn);
        vm.regs = insn.op.eval(insn.a, insn.b, insn.c, vm.regs);
        // eprintln!("[{:?}]", vm.regs);
        vm.regs.r[vm.ip_reg] += 1;
    }
    eprintln!("Register 0 at halt is {}", vm.regs.r[0]);
    vm.regs.r[0]
}

fn main() {
    run_problem(std::fs::File::open("program.txt").unwrap());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn given() {
        assert_eq!(run_problem(std::io::Cursor::new(r#"#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5"#)),
    7,);
    }
}