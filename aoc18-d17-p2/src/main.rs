pub mod problem;
use crate::problem::*;

extern crate minifb;
use minifb::{Key, KeyRepeat, Scale, Window, WindowOptions};

fn render_chart(chart: &Chart, buffer: &mut Vec<u32>, width: usize, height: usize, voff: isize) {
    for i in buffer.iter_mut() {
        *i = 0;
    }
    let iwidth = width as isize;
    let iheight = height as isize;
    let set_pixel = |buf: &mut Vec<u32>, (x, y): Coord, clr| {
        if x >= 0 && x < iwidth && y >= 0 && y < iheight {
            buf[y as usize * width + x as usize] = clr;
        }
    };

    let viewport = (2, 2 - voff);

    for coord in chart.bbox.iter() {
        let rel = relative_coord(coord, chart.bbox.top_left());
        if let Some(color) = chart.stage.get(rel).map(|cell| match cell {
            Cell::Void => 0x101010u32,
            Cell::Wall => 0x272727u32,
            Cell::StandingWater => 0x00207200u32,
            Cell::FlowingWater => 0x9D9DCDu32,
        }) {
            let rel = offset_coord(rel, viewport);
            set_pixel(buffer, rel, color);
        }
    }
}

use std::sync::mpsc;

fn main() {
    let (tx, rx) = mpsc::channel();
    std::thread::spawn(move || {
        run_problem(std::fs::File::open("input.txt").unwrap(), |chart| {
            tx.send(chart.clone()).unwrap()
        });
    });

    const WIDTH: usize = 250;
    const HEIGHT: usize = 250;
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut window = Window::new(
        "Test - ESC to exit",
        WIDTH,
        HEIGHT,
        WindowOptions {
            borderless: false,
            title: true,
            resize: false,
            scale: Scale::X4,
        },
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    let mut history = Vec::new();
    let mut shown_history: Option<usize> = None;
    let mut wanted_history: Option<usize> = None;
    let mut voff = 0isize;
    let mut follow = true;

    while window.is_open() && !window.is_key_down(Key::Escape) {
        if window.is_key_pressed(Key::Right, KeyRepeat::Yes) && !history.is_empty() {
            wanted_history = wanted_history
                .map(|cur| (cur + 1).min(history.len() - 1))
                .or(Some(0));
        }

        if window.is_key_pressed(Key::Left, KeyRepeat::Yes) && !history.is_empty() {
            follow = false;
            wanted_history = wanted_history.map(|cur| cur.saturating_sub(1)).or(Some(0));
        }

        if window.is_key_pressed(Key::Home, KeyRepeat::No) && !history.is_empty() {
            follow = false;
            wanted_history = Some(0);
        }

        if window.is_key_pressed(Key::End, KeyRepeat::No) && !history.is_empty() {
            follow = false;
            wanted_history = Some(history.len() - 1);
        }

        if window.is_key_pressed(Key::F, KeyRepeat::No) && !history.is_empty() {
            follow = !follow;
        }

        if window.is_key_pressed(Key::PageUp, KeyRepeat::Yes) && !history.is_empty() {
            voff = (voff - 100).max(0);
            shown_history = None;
        }

        if window.is_key_pressed(Key::PageDown, KeyRepeat::Yes) && !history.is_empty() {
            voff += 100;
            shown_history = None;
        }

        if let Some(scroll) = window.get_scroll_wheel() {
            voff -= scroll.1 as isize;
            shown_history = None;
        }

        if let Ok(chart) = rx.try_recv() {
            history.push(chart);
            if wanted_history.is_none() {
                wanted_history = Some(0);
            }
        }

        if follow {
            if !history.is_empty() {
                wanted_history = Some(history.len() - 1);
            }
        }

        if shown_history != wanted_history {
            render_chart(
                &history[wanted_history.unwrap()],
                &mut buffer,
                WIDTH,
                HEIGHT,
                voff,
            );
            shown_history = wanted_history;
        }

        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window.update_with_buffer(&buffer).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn given() {
        let res = run_problem(
            std::io::Cursor::new(
                r#"x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504"#,
            ),
            |_| {},
        );
        assert_eq!(res, 57);
    }
}
