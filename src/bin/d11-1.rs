fn run_problem(serial: usize) -> (isize, (usize, usize)) {
    let width = 300 + 1;
    let height = 300 + 1;
    let mut grid = Vec::with_capacity(height);
    grid.push(Vec::new());
    for row in 1..height {
        let mut line = Vec::with_capacity(width);
        line.push(0);
        for col in 1..width {
            let x = col as isize;
            let y = row as isize;
            let rid = x + 10;
            let inner_pl = (rid * rid * y + rid * serial as isize) % 1000;
            let pl = ((inner_pl / 100) % 10) - 5;
            let before = line[col - 1];
            let cum_sum = pl + before;
            line.push(cum_sum);
        }
        grid.push(line);
    }

    let sum_cell = |x: usize, y: usize| -> isize { 
        (0..3).map(|i| grid[y + i][x + 2] - grid[y + i][x - 1]).sum()
    };

    let mut best_power = None;
    let mut best_coord = None;
    for row in 1..(height - 3) {
        for col in 1..(height - 3) {
            let power = sum_cell(col, row);
            if best_power.is_none() || best_power.unwrap() < power {
                best_power = Some(power);
                best_coord = Some((col, row));
            }
        }
    }
    
    let ret = (best_power.unwrap(), best_coord.unwrap());
    println!("{:?}", ret);

    ret
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_18() {
        assert_eq!(run_problem(18), (29, (33, 45)));
    }

    #[test]
    fn test_42() {
        assert_eq!(run_problem(42), (30, (21, 61)));
    }
}

fn main() {
    run_problem(3463);
}
