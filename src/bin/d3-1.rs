use std::collections::HashSet;
use std::fs::File;
use std::io::{BufReader, BufRead, Read};

#[macro_use] extern crate lazy_static;
extern crate regex;

use regex::Regex;

#[derive(Debug)]
struct Claim {
    id: usize,
    left: usize,
    top: usize,
    width: usize,
    height: usize,
}

fn parse_claim(s: String) -> Claim {
    // #1397 @ 888,761: 25x24
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$").unwrap();
    }
    let caps = RE.captures(&s).unwrap();
    Claim {
        id: caps[1].parse().unwrap(),
        left: caps[2].parse().unwrap(),
        top: caps[3].parse().unwrap(),
        width: caps[4].parse().unwrap(),
        height: caps[5].parse().unwrap(),
    }
}

fn run_problem<T: Read>(fh: T) {
    let fh = BufReader::new(fh);
    let mut touched_tiles: HashSet<(usize, usize)> = HashSet::new();
    let mut collided_tiles: HashSet<(usize, usize)> = HashSet::new();
    let claims: Vec<Claim> = fh.lines().map(|l| parse_claim(l.unwrap())).collect();

    for claim in &claims {
        for y in claim.top .. (claim.top + claim.height) {
            for x in claim.left .. (claim.left + claim.width) {
                let is_new = touched_tiles.insert((x, y));
                if !is_new {
                    collided_tiles.insert((x, y));
                }
            }
        }
    }
    println!("{}", collided_tiles.len());
}

fn main() {
    run_problem(std::io::Cursor::new(r#"#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2"#));
    run_problem(File::open("in/d3-1.txt").unwrap());
}