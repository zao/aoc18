use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Read};

extern crate regex;
use regex::Regex;

#[derive(Clone, Debug)]
struct Marble {
    value: usize,
    prev_ptr: usize,
    next_ptr: usize,
}

struct Game {
    scores: Vec<usize>,
    field: Vec<Marble>,
    current_marble_index: usize,
    current_player: usize,
}

impl Game {
    fn new(player_count: usize, final_points: usize) -> Self {
        let mut scores = Vec::new();
        scores.resize(player_count, 0);
        let mut field = Vec::new();
        field.resize(
            1,
            Marble {
                value: 0,
                prev_ptr: 0,
                next_ptr: 0,
            },
        );
        Self {
            scores,
            field,
            current_marble_index: 0,
            current_player: 0,
        }
    }

    fn relative_index(&self, offset: isize) -> usize {
        let mut idx = self.current_marble_index;
        for i in 0 .. offset.abs() as usize {
            if offset < 0 {
                idx = self.field[idx].prev_ptr;
            }
            else {
                idx = self.field[idx].next_ptr;
            }
        }
        idx
    }

    fn insert(&mut self, value: usize) -> () {
        if value % 23 == 0 {
            let new_idx = self.relative_index(-7);
            let prev_idx = self.field[new_idx].prev_ptr;
            let next_idx = self.field[new_idx].next_ptr;
            self.scores[self.current_player] += value + self.field[new_idx].value;
            self.field[prev_idx].next_ptr = next_idx;
            self.field[next_idx].prev_ptr = prev_idx;
            self.current_marble_index = next_idx;
        } else {
            let prev_idx = self.relative_index(1);
            let next_idx = self.field[prev_idx].next_ptr;
            let new_idx = self.field.len();
            self.field[prev_idx].next_ptr = new_idx;
            self.field[next_idx].prev_ptr = new_idx;
            self.field.push(Marble {
                value,
                prev_ptr: prev_idx,
                next_ptr: next_idx,
            });
            self.current_marble_index = new_idx;
        }
        self.current_player = (self.current_player + 1) % self.scores.len();
    }
}

fn run_problem<F: Read>(fh: F) -> usize {
    let (player_count, final_points) = (|| {
        let mut fh = BufReader::new(fh);
        let mut line = String::new();
        fh.read_line(&mut line).unwrap();
        let re = Regex::new(r#"^(\d+) players; last marble is worth (\d+) points$"#).unwrap();
        let caps = re.captures(&line).unwrap();

        (
            caps.get(1).unwrap().as_str().parse::<usize>().unwrap(),
            100 * caps.get(2).unwrap().as_str().parse::<usize>().unwrap(),
        )
    })();

    let mut game = Game::new(player_count, final_points);

    for i in 1..(final_points + 1) {
        game.insert(i);
    }

    let high_score = *game.scores.iter().max().unwrap();
    eprintln!("High score: {}", high_score);

    high_score
}

fn main() {
    run_problem(File::open("in/d9-1.txt").unwrap());
}
