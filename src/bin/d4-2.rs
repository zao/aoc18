use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufRead, Read};

#[macro_use] extern crate lazy_static;
extern crate regex;

use regex::Regex;

#[derive(Debug)]
enum LogAction {
    BeginsShift(usize),
    FallsAsleep,
    WakesUp,
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
struct Timestamp {
    year: u16,
    month: u8,
    day: u8,
    hour: u8,
    minute: u8,
}

#[derive(Debug)]
struct LogEntry {
    ts: Timestamp,
    action: LogAction,
}

fn parse_log_entry(s: String) -> LogEntry {
    // #1397 @ 888,761: 25x24
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] (.+)$").unwrap();
    }
    let caps = RE.captures(&s).unwrap();
    let ts = Timestamp {
        year: u16::from_str_radix(&caps[1], 10).unwrap(),
        month: u8::from_str_radix(&caps[2], 10).unwrap(),
        day: u8::from_str_radix(&caps[3], 10).unwrap(),
        hour: u8::from_str_radix(&caps[4], 10).unwrap(),
        minute: u8::from_str_radix(&caps[5], 10).unwrap(),
    };
    let action = match caps[6].chars().nth(0).unwrap() {
        'f' => LogAction::FallsAsleep,
        'w' => LogAction::WakesUp,
        'G' => {
            let id: usize = caps[6].split(" ").nth(1).unwrap()[1..].parse().unwrap();
            LogAction::BeginsShift(id)
        }
        _ => panic!()
    };
    LogEntry { ts, action }
}

#[derive(Clone, Debug)]
struct Day {
    asleep: Vec<bool>,
}

impl Day {
    fn new() -> Day {
        let minutes = vec![false; 60];
        Day { asleep: minutes }
    }
}

#[derive(Clone, Debug)]
struct Session {
    id: usize,
    day: Day,
    sleeps_at: Option<u8>,
    minute_tally: u8,
}

impl Session {
    fn new(id: usize) -> Session {
        Session {
            id,
            day: Day::new(),
            sleeps_at: None,
            minute_tally: 0,
        }
    }
}

#[derive(Debug, Default)]
struct Watch {
    days: Vec<Day>,
    minute_tally: usize,
}

struct LogConsumer {
    current_session: Option<Session>,
    watches_by_guard: HashMap<usize, Watch>,
}

impl LogConsumer {
    fn new() -> LogConsumer {
        LogConsumer {
            current_session: None,
            watches_by_guard: HashMap::new(),
        }
    }

    fn close_shift(&mut self) {
        if let Some(session) = &self.current_session {
            let watch = self.watches_by_guard.entry(session.id).or_default();
            watch.days.push(session.day.clone());
            watch.minute_tally += session.minute_tally as usize;
        }
        self.current_session = None;
    }

    fn observe_entry(&mut self, e: Option<&LogEntry>) {
        match e {
            Some(e) => match e.action {
                LogAction::BeginsShift(id) => {
                    self.close_shift();
                    self.current_session = Some(Session::new(id));
                }
                LogAction::FallsAsleep => {
                    self.current_session.as_mut().map(|s| s.sleeps_at = Some(e.ts.minute));
                }
                LogAction::WakesUp => {
                    self.current_session.as_mut().map(|s| {
                        let from = s.sleeps_at.unwrap();
                        let to = e.ts.minute;
                        for i in from..to {
                            s.day.asleep[i as usize] = true;
                        }
                        s.minute_tally += to - from;
                    });
                }
            }
            None => self.close_shift(),
        }
    }
}

fn run_problem<T: Read>(fh: T) {
    let fh = BufReader::new(fh);
    let mut log_entries: Vec<LogEntry> = fh.lines().map(|l| parse_log_entry(l.unwrap())).collect();
    log_entries.sort_by(|a, b| a.ts.cmp(&b.ts));
    let log_entries: Vec<&LogEntry> = log_entries.iter().collect();

    let mut log_consumer = LogConsumer::new();
    for entry in log_entries {
        log_consumer.observe_entry(Some(&entry));
    }
    log_consumer.observe_entry(None);
    let mut heatmap: Vec<HashMap<usize, usize>> = Vec::new();
    heatmap.resize(60, HashMap::new());
    for (id, watch) in &log_consumer.watches_by_guard {
        for day in &watch.days {
            for (min, asleep) in day.asleep.iter().enumerate() {
                if *asleep {
                    let count = heatmap[min].entry(*id).or_default();
                    *count += 1;
                }
            }
        }
    }
    let best_guard = heatmap.iter().enumerate().filter(|(min, guards)| !guards.is_empty()).map(|(min, guards)| {
        let best = guards.iter().max_by_key(|g| g.1).unwrap();
        (min, best.0, best.1)
    }).max_by_key(|(min, id, amt)| *amt).unwrap();
    eprintln!("Minute {}, guard {} => answer {}", best_guard.0, best_guard.1, best_guard.0 * best_guard.1);
}

fn main() {
    run_problem(File::open("in/d4-1.txt").unwrap());
}