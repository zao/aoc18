struct Scoreboard {
    scores: Vec<u8>,
    elf1: usize,
    elf2: usize,
}

impl Scoreboard {
    fn new() -> Scoreboard {
        Scoreboard {
            scores: vec![3, 7],
            elf1: 0,
            elf2: 1,
        }
    }

    fn tick(&mut self) {
        let e1 = self.scores[self.elf1];
        let e2 = self.scores[self.elf2];
        let next_score = e1 + e2;
        let tens = next_score % 100 / 10;
        let ones = next_score % 10;
        if tens > 0 {
            self.scores.push(tens);
        }
        self.scores.push(ones);
        self.elf1 = (self.elf1 + 1 + e1 as usize) % self.scores.len();
        self.elf2 = (self.elf2 + 1 + e2 as usize) % self.scores.len();
    }
}

fn run_problem(input: &str) -> usize {
    let query = input
        .as_bytes()
        .iter()
        .map(|b| b - b'0')
        .collect::<Vec<u8>>();

    let mut scoreboard = Scoreboard::new();
    while scoreboard.scores.len() < query.len() {
        scoreboard.tick();
    }

    let mut window_start = 0;
    loop {
        let end = scoreboard.scores.len();
        let index_of_sequence = (&scoreboard.scores[window_start..end])
            .windows(query.len())
            .position(|w| w == &query[..]);
        if let Some(idx) = index_of_sequence {
            let ret = window_start + idx;
            eprintln!("Sequence found at {}.", ret);
            return ret;
        }
        window_start = end - query.len();
        scoreboard.tick();
    }
}

fn main() {
    run_problem("409551");
}

#[cfg(test)]
mod tests {
    use super::run_problem;

    #[test]
    fn t9() {
        assert_eq!(run_problem("5158916779"), 9);
    }

    #[test]
    fn t5() {
        assert_eq!(run_problem("0124515891"), 5);
    }

    #[test]
    fn t18() {
        assert_eq!(run_problem("9251071085"), 18);
    }

    #[test]
    fn t2018() {
        assert_eq!(run_problem("5941429882"), 2018);
    }
}
