use std::collections::HashSet;
use std::fs::File;
use std::io::{BufReader, BufRead};

fn main() {
    let fh = File::open("in/d1-1.txt").unwrap();
    let fh = BufReader::new(&fh);
    let adjs: Vec<i64> = fh.lines().map(|line| line.unwrap().parse::<i64>().unwrap()).collect();
    let mut sum = 0i64;
    let mut dup_set: HashSet<i64> = HashSet::new();
    dup_set.insert(sum);
    loop {
        for adj in &adjs {
            sum += adj;
            if !dup_set.insert(sum) {
                println!("{}", sum);
                return;
            }
        }
    }
}