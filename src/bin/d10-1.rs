use std::collections::BTreeMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};

extern crate regex;
use regex::Regex;

#[derive(Clone, Debug)]
struct Point {
    x: i64,
    y: i64,
    dx: i64,
    dy: i64,
}

#[derive(Clone, Debug)]
struct Bounds {
    min_x: i64,
    max_x: i64,
    min_y: i64,
    max_y: i64,
}

impl Bounds {
    fn from_points(points: &[Point]) -> Bounds {
        Bounds {
            min_x: points.iter().min_by_key(|p| p.x).map(|p| p.x).unwrap_or(0),
            max_x: points.iter().max_by_key(|p| p.x).map(|p| p.x).unwrap_or(0),
            min_y: points.iter().min_by_key(|p| p.y).map(|p| p.y).unwrap_or(0),
            max_y: points.iter().max_by_key(|p| p.y).map(|p| p.y).unwrap_or(0),
        }
    }

    fn width(&self) -> usize { (self.max_x - self.min_x + 1) as usize }
    fn height(&self) -> usize { (self.max_y - self.min_y + 1) as usize }

    fn rel_x(&self, x: i64) -> usize { (x - self.min_x) as usize }
    fn rel_y(&self, y: i64) -> usize { (y - self.min_y) as usize }
}

fn render_point_cloud(bounds: Bounds, points: &[Point]) -> String {
    let mut lines: Vec<Vec<u8>> = Vec::new();
    for i in 0..bounds.height() {
        let mut s = Vec::new();
        s.resize(bounds.width() as usize, b' ');
        lines.push(s);
    }
    for p in points {
        lines[bounds.rel_y(p.y)][bounds.rel_x(p.x)] = b'#';
    }

    let mut ret = String::with_capacity(bounds.width() * (1 + bounds.height()));
    for line in lines {
        ret.push_str(std::str::from_utf8(&line[..]).unwrap());
        ret.push('\n');
    }

    ret
}

fn run_problem<F: Read>(fh: F) {
    let fh = BufReader::new(fh);
    let re = Regex::new(r#"^position=<\s*(-?\d+),\s*(-?\d+)> velocity=<\s*(-?\d+),\s*(-?\d+)>$"#)
        .unwrap();
    let mut points: Vec<_> = fh
        .lines()
        .map(|line| {
            let line = line.unwrap();
            let caps = re.captures(&line).unwrap();
            Point {
                x: caps.get(1).unwrap().as_str().parse::<i64>().unwrap(),
                y: caps.get(2).unwrap().as_str().parse::<i64>().unwrap(),
                dx: caps.get(3).unwrap().as_str().parse::<i64>().unwrap(),
                dy: caps.get(4).unwrap().as_str().parse::<i64>().unwrap(),
            }
        }).collect();
    loop {
        for p in &mut points {
            p.x += p.dx;
            p.y += p.dy;
        }
        let bounds = Bounds::from_points(&points[..]);
        if bounds.width() < 80 {
            eprintln!("{:?}", bounds);
            println!("{}", render_point_cloud(bounds, &points[..]));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::io::Cursor;
    #[test]
    fn test_hi() {
        run_problem(Cursor::new(
            r#"position=< 9,  1> velocity=< 0,  2>
position=< 7,  0> velocity=<-1,  0>
position=< 3, -2> velocity=<-1,  1>
position=< 6, 10> velocity=<-2, -1>
position=< 2, -4> velocity=< 2,  2>
position=<-6, 10> velocity=< 2, -2>
position=< 1,  8> velocity=< 1, -1>
position=< 1,  7> velocity=< 1,  0>
position=<-3, 11> velocity=< 1, -2>
position=< 7,  6> velocity=<-1, -1>
position=<-2,  3> velocity=< 1,  0>
position=<-4,  3> velocity=< 2,  0>
position=<10, -3> velocity=<-1,  1>
position=< 5, 11> velocity=< 1, -2>
position=< 4,  7> velocity=< 0, -1>
position=< 8, -2> velocity=< 0,  1>
position=<15,  0> velocity=<-2,  0>
position=< 1,  6> velocity=< 1,  0>
position=< 8,  9> velocity=< 0, -1>
position=< 3,  3> velocity=<-1,  1>
position=< 0,  5> velocity=< 0, -1>
position=<-2,  2> velocity=< 2,  0>
position=< 5, -2> velocity=< 1,  2>
position=< 1,  4> velocity=< 2,  1>
position=<-2,  7> velocity=< 2, -2>
position=< 3,  6> velocity=<-1, -1>
position=< 5,  0> velocity=< 1,  0>
position=<-6,  0> velocity=< 2,  0>
position=< 5,  9> velocity=< 1, -2>
position=<14,  7> velocity=<-2,  0>
position=<-3,  6> velocity=< 2, -1>"#,
        ));
    }
}

fn main() {
    run_problem(File::open("in/d10-1.txt").unwrap());
}
