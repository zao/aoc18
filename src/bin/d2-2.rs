use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn test_words(a: &str, b: &str) -> Option<usize> {
    let n = a.len();
    for pos in 0..n {
        if a[0..pos] == b[0..pos] && a[(pos + 1)..n] == b[(pos + 1)..n] {
            return Some(pos);
        }
    }
    None
}

fn main() {
    let fh = BufReader::new(File::open("in/d2-1.txt").unwrap());
    let all_words: Vec<String> = fh.lines().map(|l| l.unwrap()).collect();
    let n = all_words.len();
    for i in 0..n {
        for j in (i + 1)..n {
            let a = &all_words[i];
            let b = &all_words[j];
            if let Some(off) = test_words(a, b) {
                println!("{}{}", &a[0..off], &a[(off + 1)..(a.len())]);
            }
        }
    }
}
