fn run_problem(serial: usize) -> (isize, (usize, usize), usize) {
    let width = 300 + 1;
    let height = 300 + 1;
    let mut grid = Vec::with_capacity(height);
    grid.push(Vec::new());
    for row in 1..height {
        let mut line = Vec::with_capacity(width);
        line.push(0);
        for col in 1..width {
            let x = col as isize;
            let y = row as isize;
            let rid = x + 10;
            let inner_pl = (rid * rid * y + rid * serial as isize) % 1000;
            let pl = ((inner_pl / 100) % 10) - 5;
            let before = line[col - 1];
            let cum_sum = pl + before;
            line.push(cum_sum);
        }
        grid.push(line);
    }

    let sum_cell = |x: usize, y: usize, d: usize| -> isize { 
        (0..d).map(|i| grid[y + i][x + d - 1] - grid[y + i][x - 1]).sum()
    };

    let mut best_power = None;
    let mut best_coord = None;
    let mut best_dim = None;
    for dim in 1..300 {
        for row in 1..(height - dim) {
            for col in 1..(height - dim) {
                let power = sum_cell(col, row, dim);
                if best_power.is_none() || best_power.unwrap() < power {
                    best_power = Some(power);
                    best_coord = Some((col, row));
                    best_dim = Some(dim);
                }
            }
        }
    }
    
    let ret = (best_power.unwrap(), best_coord.unwrap(), best_dim.unwrap());
    println!("{:?}", ret);

    ret
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_18() {
        assert_eq!(run_problem(18), (113, (90, 269), 16));
    }

    #[test]
    fn test_42() {
        assert_eq!(run_problem(42), (119, (232, 251), 12));
    }
}

fn main() {
    run_problem(3463);
}
