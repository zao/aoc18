use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Read};

fn scan_tree(nums: &[u64]) -> (u64, usize) {
    let child_count = nums[0] as usize;
    let metadata_count = nums[1] as usize;
    eprintln!(
        "Node with {} children and {} metadata items.",
        child_count, metadata_count
    );
    let mut sum = 0;
    let mut start = 2;
    let mut child_sums = Vec::new();
    for i in 0..child_count {
        let (sub_sum, tail) = scan_tree(&nums[start..]);
        start += tail;
        child_sums.push(sub_sum);
    }
    let metas = &nums[start .. (start + metadata_count)];
    if child_count == 0 {
        let sub_sum: u64 = metas.iter().sum();
        sum += sub_sum;
    }
    else {
        let sub_sum: u64 = metas.iter().map(|meta| -> u64 {
            match meta {
                &n if n >= 1 && n < (child_count as u64 + 1) => child_sums[n as usize - 1],
                _ => 0,
            }
        }).sum();
        sum += sub_sum;
    }

    (sum, start + metadata_count)
}

fn run_problem<F: Read>(fh: F) -> u64 {
    let mut fh = BufReader::new(fh);
    let mut serial = String::new();
    fh.read_line(&mut serial).unwrap();

    let nums: Vec<_> = serial
        .split(" ")
        .map(|tok| tok.parse::<u64>().unwrap())
        .collect();

    let checksum = scan_tree(&nums[..]).0;
    eprintln!("Checksum: {}", checksum);

    checksum
}

#[test]
fn sample_input() {
    assert_eq!(
        run_problem(Cursor::new("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2")),
        66
    );
}

fn main() {
    run_problem(File::open("in/d8-1.txt").unwrap());
}
