use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufRead};

fn main() {
    let fh = BufReader::new(File::open("in/d2-1.txt").unwrap());
    let mut count2 = 0;
    let mut count3 = 0;
    for word in fh.lines() {
        let mut m = HashMap::new();
        for ch in word.unwrap().chars() {
            let counter = m.entry(ch).or_insert(0);
            *counter += 1;
        }
        if m.values().any(|&n| n == 2) {
            count2 += 1;
        }
        if m.values().any(|&n| n == 3) {
            count3 += 1;
        }
    }
    println!("{}", count2 * count3);
}