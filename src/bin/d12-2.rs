use std::collections::{BTreeSet, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader, Read};

fn parse_initial_state(init: &str) -> HashSet<isize> {
    init.chars()
        .skip(15)
        .enumerate()
        .filter(|(_, ch)| *ch == '#')
        .map(|(id, _)| id as isize)
        .collect()
}

fn render(bounds: (isize, isize), bench: &HashSet<isize>) {
    let mut v = Vec::new();
    v.resize((bounds.1 - bounds.0) as usize, b'.');
    let pots: BTreeSet<isize> = bench.iter().cloned().collect();
    for val in pots {
        if val >= bounds.0 && val < bounds.1 {
            v[(val - bounds.0) as usize] = b'#';
        }
    }
    eprintln!("{}", std::str::from_utf8(&v[..]).unwrap());
}

type Bench = HashSet<isize>;

fn is_shifted_equal(bench_a: &Bench, bench_b: &Bench) -> Option<isize> {
    let reference: BTreeSet<_> = bench_a.iter().cloned().collect();
    for i in -2..3 {
        let shifted: BTreeSet<_> = bench_b.iter().map(|x| *x - i).collect();
        if shifted == reference {
            return Some(i)
        }
    }
    None
}

fn run_problem<F: Read>(fh: F, query_generation: usize) -> isize {
    let fh = BufReader::new(fh);
    let mut iter = fh.lines();
    let initial_state = iter.next().unwrap().unwrap();
    let iter = iter.skip(1);
    let mut rulebook = Vec::new();
    rulebook.resize(32, false);
    for rule in iter {
        let rule = rule.unwrap();
        let key: u8 = rule
            .chars()
            .take(5)
            .enumerate()
            .map(|(i, ch)| match ch {
                '#' => 1u8 << (4 - i),
                _ => 0u8,
            }).sum();
        let result = rule.chars().nth(9) == Some('#');
        eprintln!("{:05b} -> {}", key, result);
        rulebook[key as usize] = result;
    }

    let mut last_bench = parse_initial_state(&initial_state);
    let display_size = (0, 1000);
    eprint!("Generation {:>6}: ", 0);
    render(display_size, &last_bench);
    for gen in 1..(query_generation + 1) {
        let mut new_bench = HashSet::new();
        {
            let min_x = last_bench.iter().min().unwrap();
            let max_x = last_bench.iter().max().unwrap();
            for val in (min_x - 4)..(max_x + 4 + 1) {
                let key: usize = (0..5)
                    .map(|off| {
                        let slot = val + off;
                        if last_bench.contains(&slot) {
                            1usize << (4 - off)
                        } else {
                            0
                        }
                    }).sum();
                if rulebook[key] {
                    new_bench.insert(val + 2);
                }
            }
            if let Some(shift) = is_shifted_equal(&last_bench, &new_bench) {
                eprintln!("Generation {} is the same as the previous generation, shifted by {} slots", gen, shift);
                let num_pots = new_bench.len() as isize;
                let generations_to_go = (query_generation - gen) as isize;
                let new_sum: isize = new_bench.iter().sum();
                let ret = new_sum + num_pots * generations_to_go;
                println!("Sum is {}.", ret);
                return ret;
            }
        }
        last_bench = new_bench;
        eprint!("Generation {:>6}: ", gen);
        render(display_size, &last_bench);
    }

    let ret = last_bench.iter().sum();
    println!("Sum is {}.", ret);
    ret
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn test_sample() {
        assert_eq!(
            run_problem(
                Cursor::new(
                    r#"initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #"#
                ),
                20
            ),
            325
        );
    }
}

fn main() {
    run_problem(File::open("in/d12-1.txt").unwrap(), 50000000000);
}