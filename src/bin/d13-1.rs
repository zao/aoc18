use std::collections::{HashMap, HashSet};
use std::io::{BufRead, BufReader, Read};
use std::cmp::Ordering;

type Coord = (usize, usize);

#[derive(Clone, Copy, Debug)]
enum Cell {
    NorthSouth,
    WestEast,
    ForwardDiagonal,
    BackwardDiagonal,
    Crossing,
}

#[derive(Clone, Copy, Debug)]
enum TurnChoice {
    Left,
    Straight,
    Right,
}

#[derive(Clone, Copy, Debug)]
enum Direction {
    Northbound,
    Eastbound,
    Southbound,
    Westbound,
}

impl Direction {
    fn turned(self, choice: TurnChoice) -> Direction {
        match choice {
            TurnChoice::Left => match self {
                Direction::Northbound => Direction::Westbound,
                Direction::Eastbound => Direction::Northbound,
                Direction::Southbound => Direction::Eastbound,
                Direction::Westbound => Direction::Southbound,
            },
            TurnChoice::Straight => self,
            TurnChoice::Right => match self {
                Direction::Westbound => Direction::Northbound,
                Direction::Northbound => Direction::Eastbound,
                Direction::Eastbound => Direction::Southbound,
                Direction::Southbound => Direction::Westbound,
            },
        }
    }
}

impl TurnChoice {
    fn new() -> TurnChoice {
        TurnChoice::Left
    }

    fn next(&self) -> TurnChoice {
        match self {
            TurnChoice::Left => TurnChoice::Straight,
            TurnChoice::Straight => TurnChoice::Right,
            TurnChoice::Right => TurnChoice::Left,
        }
    }
}

#[derive(Clone, Debug)]
struct Cart {
    pos: Coord,
    dir: Direction,
    turn_choice: TurnChoice,
}

#[derive(Debug)]
struct Simulation {
    cells: HashMap<Coord, Cell>,
    carts: Vec<Cart>,
    collision_tracker: HashSet<Coord>,
}

impl Simulation {
    fn new() -> Simulation {
        Simulation {
            cells: HashMap::new(),
            carts: Vec::new(),
            collision_tracker: HashSet::new(),
        }
    }
}

fn offset_coord(coord: Coord, dir: Direction) -> Coord {
    match dir {
        Direction::Westbound => (coord.0 - 1, coord.1),
        Direction::Eastbound => (coord.0 + 1, coord.1),
        Direction::Northbound => (coord.0, coord.1 - 1),
        Direction::Southbound => (coord.0, coord.1 + 1),
    }
}

fn scan_order(a: Coord, b: Coord) -> Ordering {
    (a.1, a.0).cmp(&(b.1, b.0))
}

fn run_problem<F: Read>(fh: F) -> Coord {
    let fh = BufReader::new(fh);
    let mut sim = Simulation::new();
    for (row, line) in fh.lines().enumerate() {
        for (col, ch) in line.unwrap().chars().enumerate() {
            let pos = (col, row);
            if let Some(dir) = match ch {
                '<' => Some(Direction::Westbound),
                '>' => Some(Direction::Eastbound),
                '^' => Some(Direction::Northbound),
                'v' => Some(Direction::Southbound),
                _ => None,
            } {
                sim.carts.push(Cart {
                    pos,
                    dir,
                    turn_choice: TurnChoice::new(),
                });
                sim.collision_tracker.insert(pos);
                let cell = match &dir {
                    Direction::Westbound | Direction::Eastbound => Cell::WestEast,
                    Direction::Northbound | Direction::Southbound => Cell::NorthSouth,
                };
                sim.cells.insert(pos, cell);
            }
            if let Some(cell) = match ch {
                '-' => Some(Cell::WestEast),
                '|' => Some(Cell::NorthSouth),
                '/' => Some(Cell::ForwardDiagonal),
                '\\' => Some(Cell::BackwardDiagonal),
                '+' => Some(Cell::Crossing),
                _ => None,
            } {
                sim.cells.insert(pos, cell);
            }
        }
    }
    sim.carts.sort_by(|a, b| scan_order(a.pos, b.pos));
    eprintln!(
        "{} {} {}",
        sim.cells.len(),
        sim.carts.len(),
        sim.collision_tracker.len()
    );

    loop {
        for cart in &mut sim.carts {
            let new_coord = offset_coord(cart.pos, cart.dir);
            sim.collision_tracker.remove(&cart.pos);
            if !sim.collision_tracker.insert(new_coord) {
                let ret = new_coord;
                eprintln!("Collision at {:?}.", ret);
                return ret;
            }
            cart.pos = new_coord;
            cart.dir = match sim.cells.get(&new_coord) {
                Some(Cell::ForwardDiagonal) => match cart.dir {
                    Direction::Westbound | Direction::Eastbound => cart.dir.turned(TurnChoice::Left),
                    Direction::Northbound | Direction::Southbound => cart.dir.turned(TurnChoice::Right),
                }
                Some(Cell::BackwardDiagonal) => match cart.dir {
                    Direction::Westbound | Direction::Eastbound => cart.dir.turned(TurnChoice::Right),
                    Direction::Northbound | Direction::Southbound => cart.dir.turned(TurnChoice::Left),
                }
                Some(Cell::Crossing) => {
                    let choice = cart.turn_choice;
                    cart.turn_choice = cart.turn_choice.next();
                    cart.dir.turned(choice)
                }
                _ => cart.dir,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;
    #[test]
    fn simple() {
        run_problem(Cursor::new(
            r#"/->-\        
|   |  /----\
| /-+--+-\  |
| | |  | v  |
\-+-/  \-+--/
  \------/   "#,
        ));
    }
}

use std::fs::File;

fn main() {
    run_problem(File::open("in/d13-1.txt").unwrap());
}
