use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Read};

type Coord = (isize, isize);

fn manhattan_distance(a: Coord, b: Coord) -> usize {
    ((a.0 - b.0).abs() + (a.1 - b.1).abs()) as usize
}

fn run_problem<T: Read>(fh: T, limit: usize) {
    let limit = limit as isize;
    let fh = BufReader::new(fh);
    let input_coords: Vec<_> = fh
        .lines()
        .map(|line| {
            let line = line.unwrap();
            let xy: Vec<usize> = line
                .split(", ")
                .map(|comp| comp.parse::<usize>().unwrap())
                .collect();
            (xy[0] as isize, xy[1] as isize)
        }).collect();
    eprintln!("{:?}", input_coords);

    let left_edge = input_coords.iter().map(|c| c.0).min().unwrap() - limit - 1;
    let right_edge = input_coords.iter().map(|c| c.0).max().unwrap() + limit + 1;
    let top_edge = input_coords.iter().map(|c| c.1).min().unwrap() - limit - 1;
    let bottom_edge = input_coords.iter().map(|c| c.1).max().unwrap() + limit + 1;

    eprintln!("{:?}", (left_edge, right_edge, top_edge, bottom_edge));

    let mut coords_under_limit = 0;
    for y in top_edge..(bottom_edge + 1) {
        for x in left_edge..(right_edge + 1) {
            let dists = input_coords.iter().map(|c| manhattan_distance((x, y), *c));
            let total: usize = dists.sum();
            if (total as isize) < limit {
                coords_under_limit += 1;
            }
        }
    }

    println!("Eligible area is {} units.", coords_under_limit);
}

fn main() {
    run_problem(
        Cursor::new(
            r#"1, 1
1, 6
8, 3
3, 4
5, 5
8, 9"#,
        ),
        32,
    );
    run_problem(File::open("in/d6-1.txt").unwrap(), 10000);
}
