use std::fs::File;
use std::io::{BufReader, BufRead};

fn main() {
    let fh = File::open("in/d1-1.txt").unwrap();
    let fh = BufReader::new(&fh);
    let adjs: Vec<i64> = fh.lines().map(|line| line.unwrap().parse::<i64>().unwrap()).collect();
    let sum: i64 = adjs.iter().sum();
    println!("{}", sum);
}