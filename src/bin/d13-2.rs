use std::cmp::Ordering;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::io::{BufRead, BufReader, Read};

type Coord = (usize, usize);

#[derive(Clone, Copy, Debug)]
enum Cell {
    NorthSouth,
    WestEast,
    ForwardDiagonal,
    BackwardDiagonal,
    Crossing,
}

#[derive(Clone, Copy, Debug)]
enum TurnChoice {
    Left,
    Straight,
    Right,
}

#[derive(Clone, Copy, Debug)]
enum Direction {
    Northbound,
    Eastbound,
    Southbound,
    Westbound,
}

impl Direction {
    fn turned(self, choice: TurnChoice) -> Direction {
        match choice {
            TurnChoice::Left => match self {
                Direction::Northbound => Direction::Westbound,
                Direction::Eastbound => Direction::Northbound,
                Direction::Southbound => Direction::Eastbound,
                Direction::Westbound => Direction::Southbound,
            },
            TurnChoice::Straight => self,
            TurnChoice::Right => match self {
                Direction::Westbound => Direction::Northbound,
                Direction::Northbound => Direction::Eastbound,
                Direction::Eastbound => Direction::Southbound,
                Direction::Southbound => Direction::Westbound,
            },
        }
    }

    fn is_horizontal(self) -> bool {
        match self {
            Direction::Westbound | Direction::Eastbound => true,
            _ => false,
        }
    }

    fn is_vertical(self) -> bool {
        match self {
            Direction::Northbound | Direction::Southbound => true,
            _ => false,
        }
    }
}

impl TurnChoice {
    fn new() -> TurnChoice {
        TurnChoice::Left
    }

    fn next(&self) -> TurnChoice {
        match self {
            TurnChoice::Left => TurnChoice::Straight,
            TurnChoice::Straight => TurnChoice::Right,
            TurnChoice::Right => TurnChoice::Left,
        }
    }
}

#[derive(Clone, Debug)]
struct Cart {
    pos: Coord,
    dir: Direction,
    // id: usize,
    turn_choice: TurnChoice,
}

#[derive(Debug)]
struct Simulation {
    cells: HashMap<Coord, Cell>,
    carts: Vec<Option<Cart>>,
}

impl Simulation {
    fn new() -> Simulation {
        Simulation {
            cells: HashMap::new(),
            carts: Vec::new(),
        }
    }

    fn render(&self) -> String {
        let min_x = self.cells.keys().map(|c| c.0).min().unwrap();
        let max_x = self.cells.keys().map(|c| c.0).max().unwrap();
        let min_y = self.cells.keys().map(|c| c.1).min().unwrap();
        let max_y = self.cells.keys().map(|c| c.1).max().unwrap();
        let width = 1 + max_x - min_x;
        let pitch = width + 1;
        let height = 1 + max_y - min_y;

        let carts_by_coord = self
            .carts
            .iter()
            .cloned()
            .filter_map(|cart| cart.map(|cart| (cart.pos, cart)))
            .collect::<HashMap<_, _>>();

        let mut buf = Vec::with_capacity(pitch * height);
        for row in 0..height {
            for col in 0..width {
                let coord = (col, row);
                buf.push(if let Some(cart) = carts_by_coord.get(&coord) {
                    match cart.dir {
                        Direction::Westbound => '<',
                        Direction::Eastbound => '>',
                        Direction::Northbound => '^',
                        Direction::Southbound => 'v',
                    }
                } else if let Some(cell) = self.cells.get(&coord) {
                    match cell {
                        Cell::NorthSouth => '|',
                        Cell::WestEast => '-',
                        Cell::ForwardDiagonal => '/',
                        Cell::BackwardDiagonal => '\\',
                        Cell::Crossing => '+',
                    }
                } else {
                    ' '
                });
            }
            buf.push('\n');
        }
        buf.iter().cloned().collect::<String>()
    }

    fn tick(&mut self) {
        // Figure out which order to move the carts
        let mut remaining_carts = self.carts.iter().enumerate().filter(|(id, cart)| cart.is_some()).map(|(id, cart)| id).collect::<Vec<_>>();
        remaining_carts.sort_by_key(|id| {
            let cart = &self.carts[*id].clone().unwrap();
            (cart.pos.1, cart.pos.0)
        });
        // Move each cart
        for id in remaining_carts {
            //  * if there's a cart in the way, clear both
            if let Some(mut cart) = self.carts[id].clone() {
                let new_pos = offset_coord(cart.pos, cart.dir);
                // eprintln!("Moving {} from {:?} to {:?}.", id, cart.pos, new_pos);
                if let Some(collider) = self.carts.iter().position(|other| match other {
                    Some(other) => other.pos == new_pos,
                    None => false,
                }) {
                    // eprintln!("Collision of {} and {} at {:?}.", id, collider, new_pos);
                    self.carts[id] = None;
                    self.carts[collider] = None;
                }
                else {
                    let new_dir = match self.cells[&new_pos] {
                        Cell::ForwardDiagonal if cart.dir.is_horizontal() => cart.dir.turned(TurnChoice::Left),
                        Cell::ForwardDiagonal if cart.dir.is_vertical() => cart.dir.turned(TurnChoice::Right),
                        Cell::BackwardDiagonal if cart.dir.is_horizontal() => cart.dir.turned(TurnChoice::Right),
                        Cell::BackwardDiagonal if cart.dir.is_vertical() => cart.dir.turned(TurnChoice::Left),
                        Cell::Crossing => {
                            let dir = cart.dir.turned(cart.turn_choice);
                            cart.turn_choice = cart.turn_choice.next();
                            dir
                        }
                        _ => cart.dir,
                    };
                    cart.pos = new_pos;
                    cart.dir = new_dir;
                    self.carts[id] = Some(cart);
                }
            }
        }
    }

    fn last_cart(&self) -> Option<Cart> {
        let remaining_carts = self.carts.iter().filter(|cart| cart.is_some()).collect::<Vec<_>>();
        if remaining_carts.len() == 1 {
            remaining_carts[0].clone()
        }
        else {
            None
        }
    }
}

fn offset_coord(coord: Coord, dir: Direction) -> Coord {
    match dir {
        Direction::Westbound => (coord.0 - 1, coord.1),
        Direction::Eastbound => (coord.0 + 1, coord.1),
        Direction::Northbound => (coord.0, coord.1 - 1),
        Direction::Southbound => (coord.0, coord.1 + 1),
    }
}

fn scan_order(a: Coord, b: Coord) -> Ordering {
    (a.1, a.0).cmp(&(b.1, b.0))
}

fn run_problem<F: Read>(fh: F) -> Coord {
    let fh = BufReader::new(fh);
    let mut sim = Simulation::new();
    let mut next_cart_id = 0;
    for (row, line) in fh.lines().enumerate() {
        for (col, ch) in line.unwrap().chars().enumerate() {
            let pos = (col, row);
            if let Some(dir) = match ch {
                '<' => Some(Direction::Westbound),
                '>' => Some(Direction::Eastbound),
                '^' => Some(Direction::Northbound),
                'v' => Some(Direction::Southbound),
                _ => None,
            } {
                let id = next_cart_id;
                next_cart_id += 1;
                sim.carts.push(Some(Cart {
                    pos,
                    dir,
                    // id,
                    turn_choice: TurnChoice::new(),
                }));
                let cell = match &dir {
                    Direction::Westbound | Direction::Eastbound => Cell::WestEast,
                    Direction::Northbound | Direction::Southbound => Cell::NorthSouth,
                };
                sim.cells.insert(pos, cell);
            }
            if let Some(cell) = match ch {
                '-' => Some(Cell::WestEast),
                '|' => Some(Cell::NorthSouth),
                '/' => Some(Cell::ForwardDiagonal),
                '\\' => Some(Cell::BackwardDiagonal),
                '+' => Some(Cell::Crossing),
                _ => None,
            } {
                sim.cells.insert(pos, cell);
            }
        }
    }

    loop {
        // eprintln!("{}", sim.render());
        if let Some(last) = sim.last_cart() {
            let ret = last.pos;
            println!("Final cart is at {:?} at the end of the tick.", ret);
            return ret;
        }
        sim.tick();
    }
    (9001, 9001)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;
    #[test]
    fn simple() {
        assert_eq!(
            run_problem(Cursor::new(
                r#"/>-<\  
|   |  
| /<+-\
| | | v
\>+</ |
  |   ^
  \<->/"#,
            )),
            (6, 4)
        );
    }

    #[test]
    fn ghost() {
        assert_eq!(
            run_problem(Cursor::new(
                r#"  v
>-<"#,
            )),
            (1, 1)
        );
    }
}

use std::fs::File;

fn main() {
    run_problem(File::open("in/d13-1.txt").unwrap());
}
