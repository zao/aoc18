use std::collections::{BinaryHeap, HashMap, HashSet};
use std::io::{BufRead, BufReader, Read, Write};

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

extern crate revord;
use revord::RevOrd;

extern crate svg;
use svg::node::element::path::Data;
use svg::node::element::Path;
use svg::Document;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize)]
enum Team {
    Elves,
    Goblins,
}

impl Team {
    fn try_new(ch: char) -> Option<Self> {
        match ch {
            'E' => Some(Team::Elves),
            'G' => Some(Team::Goblins),
            _ => None,
        }
    }

    fn is_elf(&self) -> bool {
        match self {
            Team::Elves => true,
            _ => false,
        }
    }

    fn is_goblin(&self) -> bool {
        match self {
            Team::Goblins => true,
            _ => false,
        }
    }

    fn color(&self) -> &str {
        match self {
            Team::Goblins => "green",
            Team::Elves => "red",
        }
    }
}

#[derive(Debug, PartialEq)]
struct Conclusion {
    score: usize,
    turn: usize,
    health: usize,
    winner: Team,
}

// Note, (Y, X) to sort by "reading order"
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize)]
struct Coord {
    row: isize,
    col: isize,
}

#[derive(Debug, Clone, Serialize)]
struct Pawn {
    coord: Coord,
    team: Team,
    health: usize,
}

type PawnId = usize;

#[derive(Debug, Serialize)]
struct DebugState {
    turn: usize,
    current_id: usize,
    after_actions: bool,
    final_turn: Option<usize>,
    floor: HashSet<Coord>,
    pawns: HashMap<PawnId, Pawn>,
    corner: Coord,
}

#[derive(Debug, Serialize)]
struct Battlefield {
    turn: usize,
    final_turn: Option<usize>,
    floor: HashSet<Coord>,
    pawns: HashMap<PawnId, Pawn>,
    corner: Coord,
}

// Returns the adjacent four cells in "reading order", top-down left-to-right.
fn adjacent4(c: Coord) -> Vec<Coord> {
    let Coord { row, col } = c;
    vec![
        Coord { row: row - 1, col },
        Coord { row, col: col - 1 },
        Coord { row, col: col + 1 },
        Coord { row: row + 1, col },
    ]
}

enum TileLookup {
    Pawn(usize),
    Empty,
}

impl Battlefield {
    fn new<F: BufRead>(fh: F) -> Self {
        let mut floor = HashSet::new();
        let mut pawns: HashMap<PawnId, Pawn> = HashMap::new();
        let mut corner = Coord { row: 0, col: 0 };
        fh.lines().enumerate().for_each(|(row, line)| {
            line.unwrap().chars().enumerate().for_each(|(col, ch)| {
                let coord = Coord {
                    row: row as isize,
                    col: col as isize,
                };
                corner.row = corner.row.max(coord.row);
                corner.col = corner.col.max(coord.col);
                if let Some(team) = Team::try_new(ch) {
                    let new_id = pawns.len();
                    pawns.insert(
                        new_id,
                        Pawn {
                            coord,
                            team,
                            health: 200,
                        },
                    );
                    floor.insert(coord);
                } else if ch == '.' {
                    floor.insert(coord);
                }
            });
        });
        Self {
            turn: 0,
            final_turn: None,
            floor,
            pawns,
            corner,
        }
    }

    fn alive_targets_for_team(&self, team: Team) -> Vec<PawnId> {
        self.pawns
            .iter()
            .filter(|(_, pawn)| pawn.team != team)
            .map(|(id, _)| id)
            .cloned()
            .collect()
    }

    fn occupied_tiles(&self) -> Vec<Coord> {
        self.pawns.values().map(|pawn| pawn.coord).collect()
    }

    fn lookup_tile(&self, coord: Coord) -> Option<TileLookup> {
        self.pawns
            .iter()
            .find(|(_, p)| p.coord == coord)
            .map(|(id, _)| TileLookup::Pawn(*id))
            .or_else(|| self.floor.get(&coord).map(|_| TileLookup::Empty))
    }
    
    fn shortest_ranked_path(&self, target_tiles: HashSet<Coord>, source: Coord) -> Vec<Coord> {
        let old = self.shortest_ranked_path_old(target_tiles.clone(), source);
        let new = self.shortest_ranked_path_new(target_tiles.clone(), source);
        // assert_eq!(old, new);
        new
    }

    fn shortest_ranked_path_new(&self, mut target_tiles: HashSet<Coord>, source: Coord) -> Vec<Coord> {
        let mut wavefront = BinaryHeap::new();
        wavefront.push(RevOrd((0, source)));

        let mut distances = HashMap::new();
        let mut found_targets: Vec<(usize, Coord)> = Vec::new();
        loop {
            if target_tiles.is_empty() || wavefront.is_empty() {
                break;
            }
            let (dist, node) = wavefront.pop().unwrap().0;
            if target_tiles.contains(&node) {
                target_tiles.remove(&node);
                found_targets.push((dist, node));
            } else if !distances.contains_key(&node) {
                for coord in adjacent4(node) {
                    if let Some(TileLookup::Empty) = self.lookup_tile(coord) {
                        wavefront.push(RevOrd((dist + 1, coord)));
                    }
                }
            }
            distances.entry(node).or_insert(dist);
        }

        match found_targets.iter().min() {
            None => vec![],
            Some(t) => {
                let mut backtrack: Vec<(usize, Coord)> = Vec::new();
                backtrack.push(*t);
                loop {
                    let head = backtrack.last().unwrap().clone();
                    if head.0 == 1 {
                        let mut ret: Vec<_> = backtrack.iter().map(|t| t.1).collect();
                        ret.reverse();
                        break ret
                    }
                    let a4 = adjacent4(head.1);
                    for n in a4 {
                        if let Some(&dist) = distances.get(&n) {
                            if dist < head.0 {
                                backtrack.push((dist, n));
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    fn shortest_ranked_path_old(&self, mut target_tiles: HashSet<Coord>, source: Coord) -> Vec<Coord> {
        let mut wavefront = BinaryHeap::new();
        wavefront.push(RevOrd((0, source)));

        let mut distances = HashMap::new();
        let mut found_targets: Vec<(usize, Coord)> = Vec::new();
        loop {
            if target_tiles.is_empty() || wavefront.is_empty() {
                break;
            }
            let (dist, node) = wavefront.pop().unwrap().0;
            if target_tiles.contains(&node) {
                target_tiles.remove(&node);
                found_targets.push((dist, node));
            } else if !distances.contains_key(&node) {
                for coord in adjacent4(node) {
                    if let Some(TileLookup::Empty) = self.lookup_tile(coord) {
                        wavefront.push(RevOrd((dist + 1, coord)));
                    }
                }
            }
            distances.entry(node).or_insert(dist);
        }

        match found_targets.iter().min() {
            None => vec![],
            Some(t) => {
                let mut backtrack: Vec<(usize, Coord)> = Vec::new();
                backtrack.push(*t);
                loop {
                    let head = backtrack.last().unwrap().clone();
                    if head.0 == 1 {
                        let mut ret: Vec<_> = backtrack.iter().map(|t| t.1).collect();
                        ret.reverse();
                        break ret
                    }
                    let a4 = adjacent4(head.1);
                    for n in a4 {
                        if let Some(&dist) = distances.get(&n) {
                            if dist < head.0 {
                                backtrack.push((dist, n));
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    fn find_adjacent_targets(&self, id: PawnId, target_ids: &[PawnId]) -> Vec<PawnId> {
        let pawn = self.pawns[&id].clone();
        let a4 = adjacent4(pawn.coord);
        a4.iter()
            .map(|c| target_ids.iter().find(|pid| self.pawns[pid].coord == *c))
            .filter(|x| x.is_some())
            .map(|x| *x.unwrap())
            .collect()
    }

    fn attack_power_for(&self, _team: Team) -> usize {
        3
    }

    fn debug_state(&self, current_id: PawnId, after_actions: bool) -> DebugState {
        DebugState {
            turn: self.turn,
            current_id,
            after_actions,
            final_turn: self.final_turn,
            floor: self.floor.clone(),
            pawns: self.pawns.clone(),
            corner: self.corner,
        }
    }

    fn as_svg_document(&self) -> svg::Document {
        let right = (self.corner.col + 1) * 10;
        let bottom = (self.corner.row + 1) * 10;
        let mut doc = Document::new()
            .set("viewbox", (0, 0, right * 10, bottom * 10))
            .set("width", right)
            .set("height", bottom)
            .set("preserveAspectRatio", "xMidYMid");

        let mut root = svg::node::element::Group::new().set("transform", "scale(10)");

        root = self.floor.iter().fold(root, |r, c| {
            let node = svg::node::element::Rectangle::new()
                .set("x", c.col)
                .set("y", c.row)
                .set("width", 0.95)
                .set("height", 0.95)
                .set("rx", 0.15)
                .set("ry", 0.15)
                .set("style", "fill: gray");
            r.add(node)
        });

        root = self.pawns.iter().fold(root, |r, (id, p)| {
            let cx = p.coord.col as f32 + 0.5;
            let cy = p.coord.row as f32 + 0.5;
            r.add(
                svg::node::element::Circle::new()
                    .set("cx", cx)
                    .set("cy", cy)
                    .set("r", 0.4)
                    .set("style", format!("fill: {}", p.team.color())),
            )
        });

        doc.add(root)
    }

    fn tick_round(&mut self, tracer: &mut Tracer) {
        let mut pawns_alive = self.pawns.keys().cloned().collect::<Vec<_>>();
        pawns_alive.sort_by(|a, b| self.pawns[a].coord.cmp(&self.pawns[b].coord));

        for id in pawns_alive {
            tracer.trace(self, id, false);
            if self.pawns.contains_key(&id) {
                // Pawn is still alive.
                let pawn = self.pawns[&id].clone();

                // Check for adjacent targets.
                let targets = self.alive_targets_for_team(pawn.team);
                if targets.is_empty() {
                    self.final_turn = Some(self.turn);
                    break;
                }
                let mut adjacent_targets = self.find_adjacent_targets(id, &targets);
                if adjacent_targets.is_empty() {
                    // If none, try to move toward one.
                    let all_occupied = self.occupied_tiles();

                    let open_target_neighbours =
                        targets.iter().fold(HashSet::new(), |mut h, pid| {
                            if *pid != id {
                                for coord in adjacent4(self.pawns[pid].coord) {
                                    if !all_occupied.contains(&coord) {
                                        h.insert(coord);
                                    }
                                }
                            }
                            h
                        });

                    // See if there's any decent path to take toward a foe.
                    let best_path = self.shortest_ranked_path(open_target_neighbours, pawn.coord);
                    if let Some(next) = best_path.get(0) {
                        // Actually move to the next step in the path.
                        self.pawns.get_mut(&id).unwrap().coord = *next;
                    }

                    // Recheck for any new targets after moving.
                    adjacent_targets = self.find_adjacent_targets(id, &targets);
                }

                if !adjacent_targets.is_empty() {
                    let best = adjacent_targets
                        .iter()
                        .min_by_key(|pid| {
                            let pawn = &self.pawns[pid];
                            (pawn.health, pawn.coord)
                        })
                        .unwrap();
                    let new_health = self.pawns[best]
                        .health
                        .saturating_sub(self.attack_power_for(pawn.team));
                    if new_health > 0 {
                        self.pawns.get_mut(best).unwrap().health = new_health;
                    } else {
                        self.pawns.remove(best);
                    }
                }
            }
            tracer.trace(self, id, true);
        }
        self.turn += 1;
    }

    fn conclusion(&self) -> Option<Conclusion> {
        if let Some(turn) = self.final_turn {
            let winner = self.pawns.iter().nth(0).unwrap().1.team;
            let health = self.pawns.values().fold(0, |acc, p| acc + p.health);
            let score = health * turn;
            eprintln!("Score is {}, {} health * {} turns.", score, health, turn);
            Some(Conclusion {
                score,
                turn,
                health,
                winner,
            })
        } else {
            None
        }
    }
}

struct Tracer {
    traces: Vec<DebugState>,
}

impl Tracer {
    fn new() -> Self {
        Self {
            traces: Vec::new(),
        }
    }

    fn trace(&mut self, bf: &Battlefield, current_id: PawnId, after_actions: bool) {
        self.traces.push(bf.debug_state(current_id, after_actions));
    }
    
    fn render(&self) -> String {
        serde_json::to_string(&self.traces).unwrap()
    }
}

fn run_problem<F: Read>(fh: F) -> Conclusion {
    let mut bf = Battlefield::new(BufReader::new(fh));
    let mut tracer = Tracer::new();
    loop {
        std::fs::write("out/d15-1.json", tracer.render()).unwrap();
        if let Some(conclusion) = bf.conclusion() {
            eprintln!(
                "The battle has concluded, with a score of {}",
                conclusion.score
            );
            return conclusion;
        }

        bf.tick_round(&mut tracer);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t1() {
        assert_eq!(
            run_problem(std::io::Cursor::new(
                r#"#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######"#
            )),
            Conclusion {
                score: 27730,
                turn: 47,
                health: 590,
                winner: Team::Goblins
            }
        );
    }

    #[test]
    fn t2() {
        assert_eq!(
            run_problem(std::io::Cursor::new(
                r#"#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######"#
            )),
            Conclusion {
                score: 36334,
                turn: 37,
                health: 982,
                winner: Team::Elves
            }
        );
    }

    #[test]
    fn t3() {
        assert_eq!(
            run_problem(std::io::Cursor::new(
                r#"#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######"#
            )),
            Conclusion {
                score: 39514,
                turn: 46,
                health: 859,
                winner: Team::Elves
            }
        );
    }

    #[test]
    fn t4() {
        assert_eq!(
            run_problem(std::io::Cursor::new(
                r#"#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######"#
            )),
            Conclusion {
                score: 27755,
                turn: 35,
                health: 793,
                winner: Team::Goblins
            }
        );
    }

    #[test]
    fn t5() {
        assert_eq!(
            run_problem(std::io::Cursor::new(
                r#"#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######"#
            )),
            Conclusion {
                score: 28944,
                turn: 54,
                health: 536,
                winner: Team::Goblins
            }
        );
    }

    #[test]
    fn t6() {
        assert_eq!(
            run_problem(std::io::Cursor::new(
                r#"#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########"#
            )),
            Conclusion {
                score: 18740,
                turn: 20,
                health: 937,
                winner: Team::Goblins
            }
        );
    }
}

fn main() {
    run_problem(std::fs::File::open("in/d15-1.txt").unwrap());
}
