use std::collections::{BTreeSet, BinaryHeap, HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Read};

extern crate revord;
use revord::RevOrd;

fn compute_duration(base: usize, symbol: char) -> usize {
    base + 1 + (symbol.to_ascii_lowercase() as u8 - b'a') as usize
}

fn run_problem<F: Read>(fh: F, base_duration: usize, worker_count: usize) -> usize {
    let fh = BufReader::new(fh);
    let mut deps: Vec<_> = fh
        .lines()
        .map(|line| {
            let line = line.unwrap();
            let mut chunks = line.split(" ");
            let req = chunks.nth(1).unwrap();
            let next = chunks.nth(5).unwrap();
            (req.chars().next().unwrap(), next.chars().next().unwrap())
        }).collect();
    
    let mut unscheduled_work: BTreeSet<char> = BTreeSet::new();
    for dep in &deps {
        unscheduled_work.insert(dep.0);
        unscheduled_work.insert(dep.1);
    }

    let mut ordering: Vec<char> = Vec::new();
    let mut free_sources: BTreeSet<char> = BTreeSet::new();
    let mut active_work: BinaryHeap<(RevOrd<usize>, char)> = BinaryHeap::new();
    let mut pending_work: BTreeSet<char> = BTreeSet::new();
    let mut t = 0;

    eprintln!("dependencies: {:?}", deps);
    while !unscheduled_work.is_empty() || !active_work.is_empty() {
        // Figure out if there's any new work and schedule as much as possible.
        // Work is ready if it is unscheduled and there are no incoming edges.
        eprintln!("t: {}", t);
        eprintln!("unscheduled work: {:?}", unscheduled_work);
        eprintln!("active work count: {}", active_work.len());
        eprintln!("pending work: {:?}", pending_work);
        let mut eligible_work: BinaryHeap<RevOrd<char>> = unscheduled_work
            .iter()
            .filter(|&id| deps.iter().all(|d| d.1 != *id))
            .map(|id| RevOrd(*id))
            .collect();
        let debug_info: Vec<char> = eligible_work.iter().map(|RevOrd(x)| *x).collect();
        eprintln!("eligible: {:?}", debug_info);
        while active_work.len() < worker_count && !eligible_work.is_empty() {
            let work = eligible_work.pop().unwrap().0;
            let completion_time = t + compute_duration(base_duration, work);
            unscheduled_work.remove(&work);
            eprintln!("Scheduling work {} for time {}.", work, completion_time);
            active_work.push((RevOrd(completion_time), work));
        }

        // Step time and complete scheduled work for that time.
        if let Some((RevOrd(eta), id)) = active_work.pop() {
            t = eta;
            let mut to_remove = vec!(id);
            loop {
                if let Some(&(RevOrd(eta), id)) = active_work.peek() {
                    if eta == t {
                        active_work.pop();
                        to_remove.push(id);
                        continue;
                    }
                }
                break;
            }
            eprintln!("to remove: {:?}", to_remove);
            eprintln!("deps: {:?}", deps);
            deps.retain(|d| !to_remove.contains(&d.0));
        }
    }

    println!("Completion time is {}", t);
    t
}

#[test]
fn sample_input() {
    assert_eq!(
        run_problem(
            Cursor::new(
                r#"Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."#,
            ),
            0,
            2
        ),
        15
    );
}

fn main() {
    run_problem(File::open("in/d7-1.txt").unwrap(), 60, 5);
}
