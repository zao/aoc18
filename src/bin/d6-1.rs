use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Read};

type Coord = (isize, isize);

fn manhattan_distance(a: Coord, b: Coord) -> usize {
    ((a.0 - b.0).abs() + (a.1 - b.1).abs()) as usize
}

fn id_letter(id: usize) -> char {
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        .chars()
        .nth(id)
        .or(Some('$'))
        .unwrap()
}

fn run_problem<T: Read>(fh: T) {
    let fh = BufReader::new(fh);
    let input_coords: Vec<_> = fh
        .lines()
        .map(|line| {
            let line = line.unwrap();
            let xy: Vec<usize> = line
                .split(", ")
                .map(|comp| comp.parse::<usize>().unwrap())
                .collect();
            (xy[0] as isize, xy[1] as isize)
        }).collect();
    eprintln!("{:?}", input_coords);

    let left_edge = input_coords.iter().map(|c| c.0).min().unwrap() - 1;
    let right_edge = input_coords.iter().map(|c| c.0).max().unwrap() + 1;
    let top_edge = input_coords.iter().map(|c| c.1).min().unwrap() - 1;
    let bottom_edge = input_coords.iter().map(|c| c.1).max().unwrap() + 1;
    eprintln!(
        "Board bounds with border is {}-{}, {}-{}",
        left_edge, right_edge, top_edge, bottom_edge
    );

    let mut area_by_id: HashMap<usize, usize> = HashMap::new();
    let mut infinite_ids = HashSet::new();
    for y in top_edge..(bottom_edge + 1) {
        for x in left_edge..(right_edge + 1) {
            let mut dists: Vec<_> = input_coords
                .iter()
                .map(|c| manhattan_distance((x, y), *c))
                .enumerate()
                .collect();
            dists.sort_by_key(|t| t.1);
            let ties: Vec<_> = dists.iter().take_while(|d| d.1 == dists[0].1).collect();
            if ties.len() == 1 {
                let id = ties[0].0;
                let mut letter = id_letter(id);
                if ties[0].1 > 0 {
                    letter = letter.to_ascii_lowercase();
                }
                if x == left_edge || x == right_edge || y == top_edge || y == bottom_edge {
                    infinite_ids.insert(id);
                }
                *area_by_id.entry(id).or_default() += 1;
            } else {
                if x == left_edge || x == right_edge || y == top_edge || y == bottom_edge {
                    for (id, _) in &ties {
                    }
                }
            }
        }
    }
    for inf in infinite_ids {
        area_by_id.remove(&inf);
    }
    let highest_finite_area = area_by_id.values().max().unwrap();
    println!("Highest finite area is {}.", highest_finite_area);
}

fn main() {
    run_problem(Cursor::new(
        r#"1, 1
1, 6
8, 3
3, 4
5, 5
8, 9"#,
    ));
    run_problem(File::open("in/d6-1.txt").unwrap());
}
