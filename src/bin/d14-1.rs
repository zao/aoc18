fn run_problem(input: usize) -> String {
    let mut scoreboard: Vec<u8> = vec![3, 7];
    let mut elf1 = 0;
    let mut elf2 = 1;

    let target_count = input + 10;
    while scoreboard.len() < target_count {
        let e1score = scoreboard[elf1];
        let e2score = scoreboard[elf2];
        let next_score = e1score + e2score;
        let tens = next_score % 100 / 10;
        let ones = next_score % 10;
        if tens > 0 {
            scoreboard.push(tens);
        }
        scoreboard.push(ones);
        elf1 = (elf1 + 1 + e1score as usize) % scoreboard.len();
        elf2 = (elf2 + 1 + e2score as usize) % scoreboard.len();
    }

    let ret = &scoreboard[input..(input + 10)]
        .iter()
        .map(|i| i + b'0')
        .collect::<Vec<u8>>();
    let ret = std::str::from_utf8(&ret).unwrap().to_owned();
    eprintln!("{}", ret);
    ret
}

fn main() {
    run_problem(409551);
}

#[cfg(test)]
mod tests {
    use super::run_problem;

    #[test]
    fn t9() {
        assert_eq!(run_problem(9), "5158916779");
    }

    #[test]
    fn t5() {
        assert_eq!(run_problem(5), "0124515891");
    }

    #[test]
    fn t18() {
        assert_eq!(run_problem(18), "9251071085");
    }

    #[test]
    fn t2018() {
        assert_eq!(run_problem(2018), "5941429882");
    }
}
