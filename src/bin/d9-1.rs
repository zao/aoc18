use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Read};

extern crate regex;
use regex::Regex;

struct Game {
    scores: Vec<usize>,
    field: Vec<usize>,
    current_marble_index: usize,
    current_player: usize,
}

impl Game {
    fn new(player_count: usize, final_points: usize) -> Self {
        let mut scores = Vec::new();
        scores.resize(player_count, 0);
        let mut field = Vec::new();
        field.resize(1, 0);
        Self {
            scores,
            field,
            current_marble_index: 0,
            current_player: 0
        }
    }

    fn relative_index(&self, offset: isize) -> usize {
        let cmi = self.current_marble_index as isize;
        let n = self.field.len() as isize;
        if offset < 0 && cmi < -offset {
            let rem = (cmi + offset) % n;
            (n + rem) as usize
        }
        else {
            ((cmi + offset) % n) as usize
        }
    }

    fn insert(&mut self, value: usize) -> () {
        if value % 23 == 0 {
            let slot = self.relative_index(-7);
            self.scores[self.current_player] += value + self.field[slot];
            self.field.remove(slot);
            self.current_marble_index = slot;
        }
        else {
            let plus2 = self.relative_index(1) + 1;
            self.field.insert(plus2, value);
            self.current_marble_index = plus2;
        }
        self.current_player = (self.current_player + 1) % self.scores.len();
    }
}

fn run_problem<F: Read>(fh: F) -> usize {
    let (player_count, final_points) = (|| {
        let mut fh = BufReader::new(fh);
        let mut line = String::new();
        fh.read_line(&mut line).unwrap();
        let re = Regex::new(r#"^(\d+) players; last marble is worth (\d+) points$"#).unwrap();
        let caps = re.captures(&line).unwrap();

        (
            caps.get(1).unwrap().as_str().parse::<usize>().unwrap(),
            caps.get(2).unwrap().as_str().parse::<usize>().unwrap(),
        )
    })();

    let mut game = Game::new(player_count, final_points);

    for i in 1..(final_points + 1) {
        game.insert(i);
    }

    let high_score = *game.scores.iter().max().unwrap();
    eprintln!("High score: {}", high_score);

    high_score
}

#[test]
fn sample0() {
    assert_eq!(
        run_problem(Cursor::new("9 players; last marble is worth 25 points")),
        32
    );
}

#[test]
fn sample1() {
    assert_eq!(
        run_problem(Cursor::new("10 players; last marble is worth 1618 points")),
        8317
    );
}

#[test]
fn sample2() {
    assert_eq!(
        run_problem(Cursor::new("13 players; last marble is worth 7999 points")),
        146373
    );
}

#[test]
fn sample3() {
    assert_eq!(
        run_problem(Cursor::new("17 players; last marble is worth 1104 points")),
        2764
    );
}

#[test]
fn sample4() {
    assert_eq!(
        run_problem(Cursor::new("21 players; last marble is worth 6111 points")),
        54718
    );
}

#[test]
fn sample5() {
    assert_eq!(
        run_problem(Cursor::new("30 players; last marble is worth 5807 points")),
        37305
    );
}

fn main() {
    run_problem(File::open("in/d9-1.txt").unwrap());
}
