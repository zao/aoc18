use std::collections::{BTreeSet, HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Read};

fn run_problem<F: Read>(fh: F) -> String {
    let fh = BufReader::new(fh);
    let mut deps: Vec<_> = fh
        .lines()
        .map(|line| {
            let line = line.unwrap();
            let mut chunks = line.split(" ");
            let req = chunks.nth(1).unwrap();
            let next = chunks.nth(5).unwrap();
            (req.chars().next().unwrap(), next.chars().next().unwrap())
        }).collect();

    let mut verts = HashSet::new();
    for dep in &deps {
        verts.insert(dep.0);
        verts.insert(dep.1);
    }
    
    let mut ordering: Vec<char> = Vec::new();
    let mut free_sources: BTreeSet<char> = BTreeSet::new();
    while !verts.is_empty() {
        let sources: HashSet<char> = verts.clone();
        let sinks: HashSet<char> = deps.iter().map(|t| t.1).collect();
        let mut new_free_sources: BTreeSet<char> = sources.difference(&sinks).map(|c| *c).collect();
        free_sources.append(&mut new_free_sources);

        if let Some(&lowest_free) = free_sources.iter().next() {
            ordering.push(lowest_free);
            deps.retain(|d| d.0 != lowest_free);
            free_sources.remove(&lowest_free);
            verts.remove(&lowest_free);
        }
    }

    let ret = ordering.into_iter().collect();
    println!("{}", ret);
    ret
}

#[test]
fn sample_input() {
    assert_eq!(run_problem(Cursor::new(
        r#"Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."#,
    )), "CABDFE");
}

fn main() {
    run_problem(File::open("in/d7-1.txt").unwrap());
}
