use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, BufRead, Read, Cursor};
use std::mem;

#[derive(Clone, Debug)]
enum Symbol {
    Upper(char),
    Lower(char),
}

impl Symbol {
    fn new(ch: char) -> Symbol {
        if ch.is_ascii_uppercase() {
            Symbol::Upper(ch.to_ascii_lowercase())
        }
        else {
            Symbol::Lower(ch)
        }
    }

    fn char(self) -> char {
        match self {
            Symbol::Upper(ch) => ch,
            Symbol::Lower(ch) => ch,
        }
    }

    fn opposes(&self, other: &Symbol) -> bool {
        use Symbol::*;
        match (self, other) {
            (Lower(ch1), Upper(ch2)) if ch1 == ch2 => true,
            (Upper(ch1), Lower(ch2)) if ch1 == ch2 => true,
            _ => false,
        }
    }
}

fn run_problem<T: Read>(fh: T) {
    let mut fh = BufReader::new(fh);
    let mut input = String::new();
    fh.read_to_string(&mut input).unwrap();
    let mut symbols: Vec<Symbol> = input.chars().map(Symbol::new).collect();
    let mut scratch: Vec<Symbol> = Vec::with_capacity(symbols.len());
    let mut skip_one = false;
    loop {
        for pair in symbols.windows(2) {
            if skip_one {
                skip_one = false;
                continue;
            }
            if pair[0].opposes(&pair[1]) {
                skip_one = true;
            }
            else {
                scratch.push(pair[0].clone());
            }
        }
        if !skip_one {
            scratch.push(symbols.last().unwrap().clone());
        }
        if symbols.len() == scratch.len() {
            println!("Remaining symbol count: {}", symbols.len());
            break;
        }
        mem::swap(&mut symbols, &mut scratch);
        scratch.clear();
    }
}

fn main() {
    run_problem(Cursor::new("aA"));
    run_problem(Cursor::new("abBA"));
    run_problem(Cursor::new("abAB"));
    run_problem(Cursor::new("aabAAB"));
    run_problem(Cursor::new("dabAcCaCBAcCcaDA"));
    run_problem(File::open("in/d5-1.txt").unwrap());
}