use std::io::{BufRead, BufReader, Read};

extern crate regex;
use regex::Regex;

pub type Coord = (isize, isize);

pub fn relative_coord(coord: Coord, base: Coord) -> Coord {
    (coord.0 - base.0, coord.1 - base.1)
}

pub fn offset_coord(coord: Coord, offset: Coord) -> Coord {
    (coord.0 + offset.0, coord.1 + offset.1)
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Rect {
    min_x: isize,
    max_x: isize,
    min_y: isize,
    max_y: isize,
}

pub struct RectIterator {
    r: Rect,
    x: isize,
    y: isize,
}

impl Iterator for RectIterator {
    type Item = Coord;

    fn next(&mut self) -> Option<Self::Item> {
        if self.y > self.r.max_y {
            None
        } else {
            let ret = Some((self.x, self.y));
            if self.x == self.r.max_x {
                self.x = self.r.min_x;
                self.y += 1;
            } else {
                self.x += 1;
            }
            ret
        }
    }
}

impl Rect {
    pub fn union(self, rhs: Rect) -> Rect {
        Rect {
            min_x: self.min_x.min(rhs.min_x),
            max_x: self.max_x.max(rhs.max_x),
            min_y: self.min_y.min(rhs.min_y),
            max_y: self.max_y.max(rhs.max_y),
        }
    }

    pub fn top_left(&self) -> Coord {
        (self.min_x, self.min_y)
    }

    pub fn bottom_right(&self) -> Coord {
        (self.max_x, self.max_y)
    }

    pub fn width(&self) -> usize {
        (self.max_x - self.min_x + 1) as usize
    }

    pub fn height(&self) -> usize {
        (self.max_y - self.min_y + 1) as usize
    }

    pub fn iter(&self) -> RectIterator {
        RectIterator {
            r: *self,
            x: self.min_x,
            y: self.min_y,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Grid<T> {
    pub cells: Vec<T>,
    pub width: usize,
    pub height: usize,
}

impl<T: Clone + Default> Grid<T> {
    fn new(width: usize, height: usize) -> Self {
        let mut cells = Vec::new();
        cells.resize(width * height, T::default());
        Self {
            cells,
            width,
            height,
        }
    }

    pub fn get(&self, coord: Coord) -> Option<T> {
        if coord.0 >= 0
            && coord.0 < self.width as isize
            && coord.1 >= 0
            && coord.1 < self.height as isize
        {
            Some(self.cells[coord.1 as usize * self.width + coord.0 as usize].clone())
        } else {
            None
        }
    }

    fn get_mut(&mut self, coord: Coord) -> Option<&mut T> {
        if coord.0 >= 0
            && coord.0 < self.width as isize
            && coord.1 >= 0
            && coord.1 < self.height as isize
        {
            Some(&mut self.cells[coord.1 as usize * self.width + coord.0 as usize])
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Cell {
    Void,
    Wall,
    StandingWater,
    FlowingWater,
}

impl Default for Cell {
    fn default() -> Cell {
        Cell::Void
    }
}

#[derive(Debug, Clone)]
pub struct Chart {
    pub stage: Grid<Cell>,
    pub bbox: Rect,
    pub shallowest_obstacle: isize,
    pub deepest_obstacle: isize,
}

impl Chart {
    pub fn new<F: BufRead>(fh: F) -> Chart {
        let re = Regex::new(r#"^(x|y)=(\d+), (?:x|y)=(\d+)..(\d+)$"#).unwrap();
        let veins = BufReader::new(fh)
            .lines()
            .map(|line| {
                let line = line.unwrap();
                let caps = re.captures(&line).unwrap();
                let at = caps[2].parse::<isize>().unwrap();
                let min = caps[3].parse::<isize>().unwrap();
                let max = caps[4].parse::<isize>().unwrap();
                match &caps[1] {
                    "x" => Rect {
                        min_x: at,
                        max_x: at,
                        min_y: min,
                        max_y: max,
                    },
                    "y" => Rect {
                        min_x: min,
                        max_x: max,
                        min_y: at,
                        max_y: at,
                    },
                    _ => unreachable!(),
                }
            })
            .collect::<Vec<_>>();

        let (v, vs) = veins.split_at(1);
        let mut bbox = vs.iter().cloned().fold(v[0], &Rect::union);
        let shallowest_obstacle = bbox.min_y;
        let deepest_obstacle = bbox.max_y;

        // Inflate to handle flows along outer edges and the source
        bbox.min_x = 500.min(bbox.min_x - 1);
        bbox.max_x = 500.max(bbox.max_x + 1);
        bbox.min_y = 0.min(bbox.min_y);
        bbox.max_y = 0.max(bbox.max_y);

        let mut stage = Grid::new(bbox.width(), bbox.height());
        for vein in veins {
            for coord in vein.iter() {
                stage
                    .get_mut(relative_coord(coord, bbox.top_left()))
                    .map(|c| *c = Cell::Wall);
            }
        }
        stage
            .get_mut(relative_coord((500, 0), bbox.top_left()))
            .map(|c| *c = Cell::FlowingWater);
        Chart {
            stage,
            bbox,
            shallowest_obstacle,
            deepest_obstacle,
        }
    }

    pub fn trace_down(&self, coord: Coord) -> isize {
        let sy = self.bbox.min_y.max(coord.1);
        for y in sy..=self.bbox.max_y {
            let rel = relative_coord((coord.0, y), self.bbox.top_left());
            match self.stage.get(rel) {
                Some(Cell::Void) => continue,
                None => continue,
                _ => {
                    let res = coord.1.max(y - 1);
                    return res;
                }
            }
        }
        self.bbox.max_y
    }

    pub fn get(&self, coord: Coord) -> Option<Cell> {
        let rel = relative_coord(coord, self.bbox.top_left());
        self.stage.get(rel)
    }

    fn get_mut(&mut self, coord: Coord) -> Option<&mut Cell> {
        let rel = relative_coord(coord, self.bbox.top_left());
        self.stage.get_mut(rel)
    }
}

fn above(coord: Coord) -> Coord {
    (coord.0, coord.1 - 1)
}

fn below(coord: Coord) -> Coord {
    (coord.0, coord.1 + 1)
}

fn left(coord: Coord) -> Coord {
    (coord.0 - 1, coord.1)
}

fn right(coord: Coord) -> Coord {
    (coord.0 + 1, coord.1)
}

fn blocker(c: Option<Cell>) -> bool {
    match c {
        Some(Cell::Wall) | Some(Cell::StandingWater) => true,
        _ => false,
    }
}

fn flowing(c: Option<Cell>) -> bool {
    match c {
        Some(Cell::FlowingWater) => true,
        _ => false,
    }
}

pub fn run_problem<F: Read, Trace>(fh: F, trace: Trace) -> usize
where
    Trace: Fn(&Chart),
{
    let mut chart = Chart::new(BufReader::new(fh));

    let mut iteration = 0;
    loop {
        eprintln!("iteration: {}", iteration);
        trace(&chart);

        let mut changed = false;
        let mut to_skip = 0;
        for coord in chart.bbox.iter() {
            if to_skip > 0 {
                to_skip -= 1;
                continue;
            }
            match chart.get(coord) {
                Some(Cell::StandingWater) | Some(Cell::Wall) | None => (),
                Some(Cell::Void) => {
                    if flowing(chart.get(above(coord)))
                        || flowing(chart.get(left(coord))) && blocker(chart.get(left(below(coord))))
                        || flowing(chart.get(right(coord)))
                            && blocker(chart.get(right(below(coord))))
                    {
                        chart.get_mut(coord).map(|c| *c = Cell::FlowingWater);
                        changed = true;
                    }
                }
                Some(Cell::FlowingWater) => {
                    if blocker(chart.get(below(coord))) && blocker(chart.get(left(coord))) {
                        let mut supported = true;
                        let mut end_x = coord.0;
                        for x in coord.0 + 1..=chart.bbox.max_x {
                            let c = (x, coord.1);
                            if blocker(chart.get(c)) {
                                break;
                            }
                            end_x = x;
                            if !blocker(chart.get(below(c))) {
                                supported = false;
                                break;
                            }
                        }
                        if supported {
                            for x in coord.0..=end_x {
                                chart
                                    .get_mut((x, coord.1))
                                    .map(|c| *c = Cell::StandingWater);
                            }
                            changed = true;
                            to_skip = end_x - coord.0 + 1;
                        }
                    }
                }
            }
        }

        if !changed {
            break;
        }
        iteration += 1;
    }

    trace(&chart);
    let mut flow_count = 0;
    let mut still_count = 0;
    for coord in chart.bbox.iter() {
        if coord.1 >= chart.shallowest_obstacle && coord.1 <= chart.deepest_obstacle {
            match chart.get(coord) {
                Some(Cell::FlowingWater) => flow_count += 1,
                Some(Cell::StandingWater) => still_count += 1,
                _ => (),
            }
        }
    }

    eprintln!(
        "Still count: {}, flow count: {}, total: {}",
        still_count,
        flow_count,
        still_count + flow_count
    );
    still_count + flow_count
}
