use std::collections::{BinaryHeap, HashMap, HashSet};
use std::io::{BufRead, BufReader, Read};

use regex::Regex;

#[macro_use]
extern crate strum_macros;

#[derive(Debug, Display, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash, EnumString)]
enum Team {
    #[strum(serialize = "Immune System")]
    ImmuneSystem,
    Infection,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash, EnumString)]
#[strum(serialize_all = "snake_case")]
enum AttackType {
    Bludgeoning,
    Cold,
    Fire,
    Radiation,
    Slashing,
}

#[derive(Debug, Clone)]
struct Group {
    team: Team,
    unit_count: usize,
    hit_points: usize,
    attack_damage: usize,
    attack_type: AttackType,
    immunity: HashSet<AttackType>,
    weakness: HashSet<AttackType>,
    initiative: usize,
}

impl Group {
    fn from_captures(caps: &regex::Captures, team: Team) -> Option<Group> {
        if caps.len() != 7 {
            None
        } else {
            let re = Regex::new(r#"(weak|immune) to ([\w ,]+)"#).unwrap();
            let mut immunity = HashSet::new();
            let mut weakness = HashSet::new();
            if let Some(cap) = caps.get(3) {
                for caps in re.captures_iter(cap.as_str()) {
                    match caps.get(1).unwrap().as_str() {
                        "weak" => caps.get(2).unwrap().as_str().split(", ").for_each(|x| {
                            weakness.insert(x.parse().unwrap());
                        }),
                        "immune" => caps.get(2).unwrap().as_str().split(", ").for_each(|x| {
                            immunity.insert(x.parse().unwrap());
                        }),
                        _ => return None,
                    }
                }
            }
            Some(Group {
                team,
                unit_count: caps.get(1).unwrap().as_str().parse().unwrap(),
                hit_points: caps.get(2).unwrap().as_str().parse().unwrap(),
                immunity,
                weakness,
                attack_damage: caps.get(4).unwrap().as_str().parse().unwrap(),
                attack_type: caps.get(5).unwrap().as_str().parse().unwrap(),
                initiative: caps.get(6).unwrap().as_str().parse().unwrap(),
            })
        }
    }

    fn effective_power(&self) -> usize {
        self.unit_count * self.attack_damage
    }
}

/// If an attacking group is considering two defending groups to which it would deal equal damage,
/// it chooses to target the defending group with the largest effective power; if there is still a tie,
/// it chooses the defending group with the highest initiative.
fn compute_attack_rating(attacker: &Group, defender: &Group) -> (usize, usize, usize) {
    let of_no_use = defender.immunity.contains(&attacker.attack_type);
    let very_effective = defender.weakness.contains(&attacker.attack_type);
    let multiplier = if of_no_use {
        0
    } else if very_effective {
        2
    } else {
        1
    };
    (
        multiplier * attacker.effective_power(),
        defender.effective_power(),
        defender.initiative,
    )
}

fn run_combat(
    groups: &Vec<Group>,
    group_labels: &Vec<(String, usize)>,
    immune_boost: usize,
) -> Option<(Team, usize)> {
    let mut groups = groups.clone();
    for group in &mut groups {
        if group.team == Team::ImmuneSystem {
            group.attack_damage += immune_boost;
        }
    }

    loop {
        let unit_counts_by_team =
            groups
                .iter()
                .fold(HashMap::new(), |mut h: HashMap<Team, usize>, g: &Group| {
                    if g.unit_count > 0 {
                        *h.entry(g.team).or_default() += g.unit_count;
                    }
                    h
                });
        // eprintln!("Units by team: {:?}", unit_counts_by_team);
        if unit_counts_by_team.len() == 1 {
            let (&team, &count) = unit_counts_by_team.iter().nth(0).unwrap();
            return Some((team, count));
        }

        // Target selection phase:
        let mut target_order = groups
            .iter()
            .enumerate()
            .filter_map(|(i, g)| {
                if g.unit_count > 0 {
                    Some((g.effective_power(), g.initiative, i))
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        target_order.sort_by_key(|&k| (usize::max_value() - k.0, usize::max_value() - k.1));

        let mut target_dibs: HashSet<usize> = HashSet::new();
        let mut targets: HashMap<usize, usize> = HashMap::new();
        for (_, _, aid) in &target_order {
            let attacker = &groups[*aid];
            if attacker.unit_count == 0 {
                continue;
            }
            let mut best_target = None;
            let mut best_rating = (0, 0, 0);
            for (gid, g) in groups.iter().enumerate() {
                if !target_dibs.contains(&gid) && g.unit_count > 0 {
                    if g.team != attacker.team {
                        let attack_rating = compute_attack_rating(&attacker, g);
                        // eprintln!(
                        //     "{} group {} would deal defending group {} {} damage",
                        //     group_labels[*aid].0,
                        //     group_labels[*aid].1,
                        //     group_labels[gid].1,
                        //     attack_rating.0
                        // );
                        if best_target.is_none()
                            || attack_rating.0 > 0 && best_rating < attack_rating
                        {
                            best_target = Some(gid);
                            best_rating = attack_rating;
                        }
                    }
                }
            }
            if best_target.is_some() && best_rating.0 > 0 {
                let gid = best_target.unwrap();
                target_dibs.insert(gid);
                targets.insert(*aid, gid);
            }
        }
        // eprintln!("");

        let mut combat_order = BinaryHeap::new();
        for (id, g) in groups.iter().enumerate() {
            if g.unit_count > 0 {
                combat_order.push((g.initiative, id));
            }
        }

        // Combat phase:
        let mut units_killed = 0;
        while let Some((_, aid)) = combat_order.pop() {
            if let Some(did) = targets.get(&aid) {
                let attacker = groups[aid].clone();
                let defender = &mut groups[*did];
                if attacker.unit_count > 0 && defender.unit_count > 0 {
                    let attack_rating = compute_attack_rating(&attacker, defender);
                    let damage = attack_rating.0;
                    let kills = damage / defender.hit_points;
                    let kills = defender.unit_count.min(kills);
                    defender.unit_count -= kills;
                    units_killed += kills;
                    // eprintln!(
                    //     "{} {} attacks defending group {}, killing {} units",
                    //     group_labels[aid].0, group_labels[aid].1, group_labels[*did].1, kills,
                    // );
                }
            }
        }
        // eprintln!("");

        if units_killed == 0 {
            return None;
        }
    }
}

fn run_problem<F: Read>(fh: F) {
    let groups = {
        let mut current_team = None;
        let mut groups = vec![];
        let team_re = Regex::new(r#"^(.*):$"#).unwrap();
        let group_re = Regex::new(r#"^(\d+) units each with (\d+) hit points ?(\([^)]+\))? with an attack that does (\d+) (\w+) damage at initiative (\d+)$"#).unwrap();
        for line in BufReader::new(fh).lines() {
            let line = line.unwrap();
            if let Some(caps) = team_re.captures(&line) {
                current_team = Some(caps.get(1).unwrap().as_str().parse::<Team>().unwrap());
            } else if let Some(caps) = group_re.captures(&line) {
                let group = Group::from_captures(&caps, current_team.unwrap());
                groups.push(group.unwrap());
            }
        }
        groups
    };
    let mut group_labels = Vec::new();
    groups.iter().fold((1, 1), |cnt, g| {
        let team = g.team;
        let (idx, cnt) = match team {
            Team::ImmuneSystem => (cnt.0, (cnt.0 + 1, cnt.1)),
            Team::Infection => (cnt.1, (cnt.0, cnt.1 + 1)),
        };
        group_labels.push((team.to_string(), idx));
        cnt
    });

    for boost in 44.. {
        let outcome = run_combat(&groups, &group_labels, boost);
        if let Some((winner, count)) = outcome {
            eprintln!(
                "Boost {} led to {} winning with {} units",
                boost, winner, count
            );
            if winner == Team::ImmuneSystem {
                break;
            }
        }
        else {
            eprintln!("Tie!");
        }
    }
}

fn main() {
    run_problem(std::fs::File::open("input.txt").unwrap());
    // run_problem(std::fs::File::open("example.txt").unwrap());
}
