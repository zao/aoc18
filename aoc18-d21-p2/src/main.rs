use std::collections::{BTreeMap, HashMap, HashSet};
use std::io::{BufRead, BufReader, Read};
use std::str::FromStr;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Registers {
    r: [u64; 6],
}

impl Registers {
    fn new() -> Self {
        Registers { r: [0u64; 6] }
    }
}

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
enum Op {
    AddR,
    AddI,
    MulR,
    MulI,
    BanR,
    BanI,
    BorR,
    BorI,
    SetR,
    SetI,
    GTIR,
    GTRI,
    GTRR,
    EQIR,
    EQRI,
    EQRR,
}

impl Op {
    fn eval(&self, a: u64, b: u64, c: u64, mut reg: Registers) -> Registers {
        let aidx = a as usize;
        let bidx = b as usize;
        let cidx = c as usize;
        reg.r[cidx] = match self {
            Op::AddR => reg.r[aidx] + reg.r[bidx],
            Op::AddI => reg.r[aidx] + b,
            Op::MulR => reg.r[aidx] * reg.r[bidx],
            Op::MulI => reg.r[aidx] * b,
            Op::BanR => reg.r[aidx] & reg.r[bidx],
            Op::BanI => reg.r[aidx] & b,
            Op::BorR => reg.r[aidx] | reg.r[bidx],
            Op::BorI => reg.r[aidx] | b,
            Op::SetR => reg.r[aidx],
            Op::SetI => a,
            Op::GTIR => (a > reg.r[bidx]) as u64,
            Op::GTRI => (reg.r[aidx] > b) as u64,
            Op::GTRR => (reg.r[aidx] > reg.r[bidx]) as u64,
            Op::EQIR => (a == reg.r[bidx]) as u64,
            Op::EQRI => (reg.r[aidx] == b) as u64,
            Op::EQRR => (reg.r[aidx] == reg.r[bidx]) as u64,
        };
        reg
    }
}

impl FromStr for Op {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "addr" => Ok(Op::AddR),
            "addi" => Ok(Op::AddI),
            "mulr" => Ok(Op::MulR),
            "muli" => Ok(Op::MulI),
            "banr" => Ok(Op::BanR),
            "bani" => Ok(Op::BanI),
            "borr" => Ok(Op::BorR),
            "bori" => Ok(Op::BorI),
            "setr" => Ok(Op::SetR),
            "seti" => Ok(Op::SetI),
            "gtir" => Ok(Op::GTIR),
            "gtri" => Ok(Op::GTRI),
            "gtrr" => Ok(Op::GTRR),
            "eqir" => Ok(Op::EQIR),
            "eqri" => Ok(Op::EQRI),
            "eqrr" => Ok(Op::EQRR),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Instruction {
    op: Op,
    a: u64,
    b: u64,
    c: u64,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.split(" ");
        Ok(Instruction {
            op: it.next().unwrap().parse().unwrap(),
            a: it.next().unwrap().parse().unwrap(),
            b: it.next().unwrap().parse().unwrap(),
            c: it.next().unwrap().parse().unwrap(),
        })
    }
}

struct VM {
    program: Vec<Instruction>,
    regs: Registers,
    ip_reg: usize,
    cycles: usize,
}

impl Instruction {
    fn as_c(&self, vm: &VM) -> String {
        let mut r = vec!["r0", "r1", "r2", "r3", "r4", "r5"];
        r[vm.ip_reg] = "ip";
        let Instruction { op, a, b, c } = *self;
        let ra = r.get(a as usize).or(Some(&"")).unwrap();
        let rb = r.get(b as usize).or(Some(&"")).unwrap();
        let rc = r.get(c as usize).or(Some(&"")).unwrap();
        match op {
            // AddR
            Op::AddR if c == a && b == a => format!("{rc} *= 2;", rc = rc),
            Op::AddR if c == a => format!("{rc} += {rb};", rc = rc, rb = rb),
            Op::AddR if c == b => format!("{rc} += {ra};", rc = rc, ra = ra),
            Op::AddR => format!("{rc} = {ra} + {rb};", rc = rc, ra = ra, rb = rb),
            // AddI
            Op::AddI if c == a && b == 0u64 => format!(";"),
            Op::AddI if c == a && b == 1u64 => format!("++{rc};", rc = rc),
            Op::AddI if c == a => format!("{rc} += 0x{b:X};", rc = rc, b = b),
            Op::AddI if b == 0 => format!("{rc} += {ra};", rc = rc, ra = ra),
            Op::AddI => format!("{rc} = {ra} + 0x{b:X};", rc = rc, ra = ra, b = b),
            // MulR
            Op::MulR if c == a => format!("{rc} *= {rb};", rc = rc, rb = rb),
            Op::MulR if c == b => format!("{rc} *= {ra};", rc = rc, ra = ra),
            Op::MulR => format!("{rc} = {ra} * {rb};", rc = rc, ra = ra, rb = rb),
            // MulI
            Op::MulI if c == a => format!("{rc} *= 0x{b:X};", rc = rc, b = b),
            Op::MulI => format!("{rc} = {ra} * 0x{b:X};", rc = rc, ra = ra, b = b),
            // BanR
            Op::BanR if c == a && c == b => format!(";"),
            Op::BanR if c == a => format!("{rc} &= {rb};", rc = rc, rb = rb),
            Op::BanR if c == b => format!("{rc} &= {ra};", rc = rc, ra = ra),
            Op::BanR => format!("{rc} = {ra} & {rb};", rc = rc, ra = ra, rb = rb),
            // BanI
            Op::BanI if b == 0 => format!("{rc} = 0;", rc = rc),
            Op::BanI if c == a => format!("{rc} &= 0x{b:X};", rc = rc, b = b),
            Op::BanI => format!("{rc} = {ra} & 0x{b:X};", rc = rc, ra = ra, b = b),
            // BorR
            Op::BorR if c == a && c == b => format!(";"),
            Op::BorR if c == a => format!("{rc} |= {rb};", rc = rc, rb = rb),
            Op::BorR if c == b => format!("{rc} |= {ra};", rc = rc, ra = ra),
            Op::BorR => format!("{rc} = {ra} | {rb};", rc = rc, ra = ra, rb = rb),
            // BorI
            Op::BorI if c == a => format!("{rc} |= 0x{b:X};", rc = rc, b = b),
            Op::BorI => format!("{rc} = {ra} | 0x{b:X};", rc = rc, ra = ra, b = b),
            // SetR
            Op::SetR => format!("{rc} = {ra};", rc = rc, ra = ra),
            // SetI
            Op::SetI => format!("{rc} = 0x{a:X};", rc = rc, a = a),
            // Relational
            Op::GTIR => format!("{rc} = 0x{a:X} > {rb};", rc = rc, a = a, rb = rb),
            Op::GTRI => format!("{rc} = {ra} > 0x{b:X};", rc = rc, ra = ra, b = b),
            Op::GTRR => format!("{rc} = {ra} > {rb};", rc = rc, ra = ra, rb = rb),
            Op::EQIR => format!("{rc} = 0x{a:X} == {rb};", rc = rc, a = a, rb = rb),
            Op::EQRI => format!("{rc} = {ra} == 0x{b:X};", rc = rc, ra = ra, b = b),
            Op::EQRR => format!("{rc} = {ra} == {rb};", rc = rc, ra = ra, rb = rb),
        }
    }
}

impl VM {
    fn dump_c(&self) {
        for (_i, insn) in self.program.iter().enumerate() {
            eprintln!("{}", insn.as_c(self));
        }
    }
}

fn run_problem<F: Read>(program_fh: F) -> u64 {
    let mut ip_reg = 0;
    let program: Vec<Instruction> = BufReader::new(program_fh)
        .lines()
        .filter_map(|line| {
            let line = line.unwrap();
            if line.chars().nth(0).unwrap() == '#' {
                ip_reg = line.split(' ').nth(1).unwrap().parse().unwrap();
                None
            } else {
                Some(line.parse().unwrap())
            }
        })
        .collect();

    let mut vm = VM {
        program,
        regs: Registers::new(),
        ip_reg,
        cycles: 0,
    };
    vm.regs.r[0] = 0;
    // vm.dump_c();
    let mut dup_check = HashSet::new();
    let mut first_seen = HashMap::new();
    while vm.regs.r[ip_reg] < vm.program.len() as u64 {
        vm.cycles += 1;
        let ip = vm.regs.r[vm.ip_reg] as usize;
        if ip == 0x1C {
            let key = (vm.regs.r[4], vm.regs.r[5]);
            // eprintln!("r4: 0x{:06x} r5: 0x{:06x}", key.0, key.1);
            if !dup_check.insert(key) {
                let best = first_seen.iter().max_by_key(|k| k.1).unwrap();
                eprintln!("{} entries, best r0 is {:?}", dup_check.len(), best);
                // 12869 entries, best r0 is (5310683, 2976386660)
            }
            first_seen.entry(key.0).or_insert(vm.cycles);
        }
        let insn = vm.program[ip];
        // eprint!("ip={:>2} ", ip);
        // eprint!(
        //     "[{:>8}, {:>8}, {:>8}, {:>8}, {:>8}, {:>8}] ",
        //     vm.regs.r[0], vm.regs.r[1], vm.regs.r[2], vm.regs.r[3], vm.regs.r[4], vm.regs.r[5]
        // );
        // eprint!("{:?} {:>2} {:>2} {:>2} ", insn.op, insn.a, insn.b, insn.c);
        // eprintln!("regs: {:?}", vm.regs);
        vm.regs = insn.op.eval(insn.a, insn.b, insn.c, vm.regs);
        // eprintln!(
        //     "[{:>8}, {:>8}, {:>8}, {:>8}, {:>8}, {:>8}]",
        //     vm.regs.r[0], vm.regs.r[1], vm.regs.r[2], vm.regs.r[3], vm.regs.r[4], vm.regs.r[5]
        // );
        vm.regs.r[vm.ip_reg] += 1;
    }
    eprintln!(
        "Registers at halt are {:?} after {} cycles.",
        vm.regs.r, vm.cycles
    );
    vm.regs.r[0]
}

fn main() {
    run_problem(std::fs::File::open("program.asm").unwrap());
}
